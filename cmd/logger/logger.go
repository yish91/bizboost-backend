package logger

import (
	"bizboost/cmd/settings"
	"github.com/evalphobia/logrus_sentry"
	"github.com/sirupsen/logrus"
	"time"
)

var log = logrus.New()

func Init() {
	sentryUrl := settings.Get().Secrets.SentryKey
	hook, err := logrus_sentry.NewSentryHook(sentryUrl, []logrus.Level{
		logrus.InfoLevel,
		logrus.PanicLevel,
		logrus.FatalLevel,
		logrus.ErrorLevel,
	})
	hook.StacktraceConfiguration.Enable = true
	hook.Timeout = 20 * time.Second

	if err == nil {
		log.Hooks.Add(hook)
	}
}

func GetLogger() *logrus.Logger {
	return log
}
