package cmd

import (
	hello "bizboost/internal/api/hello/router"
	loan "bizboost/internal/api/loan/router"
	user "bizboost/internal/api/user/router"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"github.com/rs/cors"
)

func NewRouter() *chi.Mux {

	router := chi.NewRouter()
	router.Use(
		render.SetContentType(render.ContentTypeJSON), // Setting content type as application/json
		cors.AllowAll().Handler,                       // Enabling CORS
		middleware.Logger,                             // Log API request calls
		middleware.RedirectSlashes,                    // Redirect slashes to no slash URL versions
		middleware.Recoverer,                          // Recover from panics without crashing server
		ValidateCaller,                                // validation of client
	)

	// attaching routes
	router = user.SetUserRoutes(router)
	router = hello.SetHelloRoutes(router)
	router = loan.SetLoanRoutes(router)

	return router
}
