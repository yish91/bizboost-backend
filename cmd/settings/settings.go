package settings

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/go-playground/validator.v9"
)

const SECRETS_FILE_LOCAL = "cmd/settings/secrets_local.json"
const ENV_LOCAL = "local"
const ENV_PREPRODUCTION = "preproduction"
const ENV_PROD = "prod"

var generalConfigs = map[string]string{
	ENV_LOCAL:         "cmd/settings/config_local.json",
	ENV_PREPRODUCTION: "cmd/settings/config_preproduction.json",
	ENV_PROD:          "cmd/settings/config_prod.json",
}

type Settings struct {
	Secrets        *Secrets
	GeneralConfigs *GeneralConfigs
}

type GeneralConfigs struct {
	JWTExpirationDelta int    `json:",omitempty" validate:"required"`
	InvestorPortalURL  string `json:",omitempty" validate:"required"`
	AdminPortalURL     string `json:",omitempty" validate:"required"`
	ApiURL             string `json:",omitempty" validate:"required"`
	DatabaseMain       string `json:",omitempty" validate:"required"`
	DatabaseAudit      string `json:",omitempty" validate:"required"`
}

type Secrets struct {
	PublicKey   string `json:",omitempty" validate:"required"`
	PrivateKey  string `json:",omitempty" validate:"required"`
	AWSAccess   string `json:",omitempty" validate:"required"`
	AWSSecret   string `json:",omitempty" validate:"required"`
	DatabaseURL string `json:",omitempty" validate:"required"`
	SendGridKey string `json:",omitempty" validate:"required"`
	SentryKey   string `json:",omitempty" validate:"required"`
}

var settings = Settings{}
var env = "local"

func Init() {
	env = os.Getenv("GO_ENV")
	if env == "" {
		env = "local"
		fmt.Println("Warning: GO_ENV value is missing, setting environment as - " + env)
	} else {
		fmt.Println("Running with env - " + env)
	}
	if err := LoadSettingsByEnv(env); err != nil {
		//If there's err in loading settings, blow-up!
		panic(err)
	}
}

func LoadSettingsByEnv(env string) error {
	secrets, err := getSecrets(env)
	if err != nil {
		return err
	}
	settings.Secrets = secrets

	generalConfigs, err := getGeneralConfigs(env)
	if err != nil {
		return err
	}
	settings.GeneralConfigs = generalConfigs

	return nil
}

func getSecrets(env string) (*Secrets, error) {
	result := &Secrets{}
	var secretsContent []byte
	if env == "local" {
		secretsFile := SECRETS_FILE_LOCAL
		content, err := ioutil.ReadFile(secretsFile)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("Error while reading config file - %s", err.Error()))
		}
		secretsContent = content
	} else {
		injectedSecrets := os.Getenv("SECRETS")
		secretsContent = []byte(injectedSecrets)
	}

	if jsonErr := json.Unmarshal(secretsContent, result); jsonErr != nil {
		return nil, errors.New(fmt.Sprintf("Error while parsing secretsContent for env[%s] - %s", env, jsonErr.Error()))
	}
	if err := validator.New().Struct(result); err != nil {
		return nil, err
	}
	return result, nil
}

func getGeneralConfigs(env string) (*GeneralConfigs, error) {
	result := &GeneralConfigs{}
	configFile := generalConfigs[env]
	content, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Error while reading config file [%s] - %s", configFile, err.Error()))
	}
	if jsonErr := json.Unmarshal(content, result); jsonErr != nil {
		return nil, errors.New(fmt.Sprintf("Error while parsing content for file[%s] - %s", configFile, jsonErr.Error()))
	}
	if err := validator.New().Struct(result); err != nil {
		return nil, err
	}
	return result, nil
}

func GetEnvironment() string {
	return env
}

func Get() Settings {
	if &settings == nil {
		Init()
	}
	return settings
}
