package handler

import (
	"bizboost/cmd/settings"
	"bizboost/internal/platform/utils"
	"fmt"
	"net/http"
	"strconv"
	"time"
)

func Hello(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("Hello, World! This is the " + settings.GetEnvironment() + " environment!"))
	if err != nil {
		fmt.Println(err)
	}
}

func Error(w http.ResponseWriter, r *http.Request) {

	_, err := strconv.Atoi("kaka")
	if err != nil {
		utils.LogError(err)
	}
	w.Write([]byte("Error"))
}

func NewError(w http.ResponseWriter, r *http.Request) {

	_, err := strconv.Atoi("kaka")
	if err != nil {
		utils.LogError(err)
	}
	w.Write([]byte("Error"))
}

func ConcurrencyTest(w http.ResponseWriter, r *http.Request) {

	go doWork(0)
	go doWork(1)

	w.Write([]byte("End"))
	return
}

func doWork(worker int) {
	time.Sleep(10000 * time.Millisecond)
	fmt.Printf("Worker %d: Jobs Done! \n", worker)
}
