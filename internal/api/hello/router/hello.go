package router

import (
	"bizboost/internal/api/hello/handler"
	"github.com/go-chi/chi"
)

func SetHelloRoutes(router *chi.Mux) *chi.Mux {

	router.Group(func(mux chi.Router) {
		// Print info about claim
		mux.Route("/test", func(mux chi.Router) {
			mux.Get("/hello", handler.Hello)
			mux.Get("/error", handler.Error)
			mux.Get("/newerror", handler.NewError)
			mux.Get("/concurrency", handler.ConcurrencyTest)
		})
	})
	return router
}
