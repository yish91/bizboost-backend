package db

import (
	"bizboost/internal/api/loan/model"
	uModel "bizboost/internal/api/user/model"
	"bizboost/internal/platform/constants"
	"bizboost/internal/platform/db"
	"context"
	"go.mongodb.org/mongo-driver/mongo/options"

	"bizboost/internal/platform/utils"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type LoanDao interface {
	CreateLoan(ctx context.Context, loan *model.Loan) error
	UpdateLoan(ctx context.Context, loan *model.Loan) error
	GetLoan(ctx context.Context, uuid string) (*model.Loan, error)
	GetLoans(ctx context.Context) ([]*model.Loan, error)
	GetLoansForBusinessOwner(ctx context.Context, userId string) ([]*model.Loan, error)
	GetClosedLoansForBusinessOwner(ctx context.Context, userId string) ([]*model.Loan, error)
	CreateBid(ctx context.Context, bid *model.Bid) error
	GetBid(ctx context.Context, uuid string) (*model.Bid, error)
	GetBids(ctx context.Context, userId string) ([]*model.Bid, error)
	GetBidsForLoans(ctx context.Context, loanUUID string) ([]*model.Bid, error)
	GetClosedBidsForLoans(ctx context.Context, loanUUID string) ([]*model.Bid, error)
	GetClosedBids(ctx context.Context, userId string) ([]*model.Bid, error)
	GetWithdrawals(ctx context.Context, userId string) ([]*model.Withdrawal, error)
	CreateWithdrawal(ctx context.Context, withdrawal *model.Withdrawal) error
	GetUserByUUID(ctx context.Context, uuid string) (*uModel.User, error)
	GetBusinessOwner(ctx context.Context, uuid string) (*uModel.BusinessOwner, error)
	CreateRepayment(ctx context.Context, repaymentSchedule *model.RepaymentSchedule) error
	GetRepayment(ctx context.Context, uuid string) (*model.RepaymentSchedule, error)

	GetDeposits(ctx context.Context, userId string) ([]*model.Deposit, error)
	CreateDeposit(ctx context.Context, withdrawal *model.Deposit) error
}

type LoanDaoImpl struct {
	Db *mongo.Database
}

func NewLoanDaoImpl(db *mongo.Database) LoanDao {
	return LoanDaoImpl{Db: db}
}

func (l LoanDaoImpl) CreateLoan(ctx context.Context, loan *model.Loan) error {
	collection := l.Db.Collection(constants.Collection_Loan)
	_, err := collection.InsertOne(ctx, loan)
	if err != nil {
		return err
	}
	return nil
}

func (l LoanDaoImpl) UpdateLoan(ctx context.Context, loan *model.Loan) error {
	idDoc := bson.D{{"uuid", loan.UUID}}
	_, err := l.Db.Collection(constants.Collection_Loan).UpdateOne(ctx, idDoc, bson.D{
		{"$set", loan},
	})
	if err != nil {
		return err
	}
	return nil
}

func (l LoanDaoImpl) GetLoan(ctx context.Context, uuid string) (*model.Loan, error) {
	filter := bson.D{{"uuid", uuid}}
	loan := &model.Loan{}
	if err := l.Db.Collection(constants.Collection_Loan).FindOne(ctx, filter).Decode(&loan); err != nil {
		return nil, err
	}
	return loan, nil
}

func (l LoanDaoImpl) GetLoans(ctx context.Context) ([]*model.Loan, error) {
	filter := bson.D{{"status", model.LOAN_STATUS_ACTIVE.String()}}
	result := []*model.Loan{}
	if err := utils.FindAll(ctx, db.DatabaseStore.GetDatabase(), constants.Collection_Loan, filter, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (l LoanDaoImpl) GetLoansForBusinessOwner(ctx context.Context, userId string) ([]*model.Loan, error) {
	filter := bson.D{{"status", model.LOAN_STATUS_ACTIVE.String()}, {"businessowner", userId}}
	result := []*model.Loan{}
	if err := utils.FindAll(ctx, db.DatabaseStore.GetDatabase(), constants.Collection_Loan, filter, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (l LoanDaoImpl) GetClosedLoansForBusinessOwner(ctx context.Context, userId string) ([]*model.Loan, error) {
	filter := bson.D{{"status", model.LOAN_STATUS_CLOSED.String()}, {"businessowner", userId}}
	result := []*model.Loan{}
	if err := utils.FindAll(ctx, db.DatabaseStore.GetDatabase(), constants.Collection_Loan, filter, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (l LoanDaoImpl) CreateBid(ctx context.Context, bid *model.Bid) error {
	collection := db.DatabaseStore.GetDatabase().Collection(constants.Collection_Bid)
	opts := options.Update()
	opts.SetUpsert(true)

	filter := bson.D{{Key: "loanuuid", Value: bid.LoanUUID}, {Key: "useruuid", Value: bid.UserUUID}}
	update := bson.D{{Key: "$set", Value: bid}}
	_, err := collection.UpdateOne(ctx, filter, update, opts)
	if err != nil {
		msg := "problem creating bid"
		return utils.WrapError(err, msg)
	}
	return nil
}

func (l LoanDaoImpl) GetBid(ctx context.Context, uuid string) (*model.Bid, error) {
	filter := bson.D{{"uuid", uuid}}
	bid := &model.Bid{}
	if err := l.Db.Collection(constants.Collection_Bid).FindOne(ctx, filter).Decode(&bid); err != nil {
		return nil, err
	}
	return bid, nil
}

func (l LoanDaoImpl) GetBids(ctx context.Context, userId string) ([]*model.Bid, error) {
	filter := bson.D{{"useruuid", userId}, {"status", model.BID_STATUS_ACTIVE.String()}}
	result := []*model.Bid{}
	if err := utils.FindAll(ctx, db.DatabaseStore.GetDatabase(), constants.Collection_Bid, filter, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (l LoanDaoImpl) GetBidsForLoans(ctx context.Context, loanUUID string) ([]*model.Bid, error) {
	filter := bson.D{{"loanuuid", loanUUID}}
	result := []*model.Bid{}
	if err := utils.FindAll(ctx, db.DatabaseStore.GetDatabase(), constants.Collection_Bid, filter, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (l LoanDaoImpl) GetClosedBidsForLoans(ctx context.Context, loanUUID string) ([]*model.Bid, error) {
	filter := bson.D{{"loanuuid", loanUUID}, {"status", model.BID_STATUS_CLOSED.String()}}
	result := []*model.Bid{}
	if err := utils.FindAll(ctx, db.DatabaseStore.GetDatabase(), constants.Collection_Bid, filter, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (l LoanDaoImpl) GetClosedBids(ctx context.Context, userId string) ([]*model.Bid, error) {
	filter := bson.D{{"useruuid", userId}, {"status", model.BID_STATUS_CLOSED.String()}}
	result := []*model.Bid{}
	if err := utils.FindAll(ctx, db.DatabaseStore.GetDatabase(), constants.Collection_Bid, filter, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (l LoanDaoImpl) GetWithdrawals(ctx context.Context, userId string) ([]*model.Withdrawal, error) {
	filter := bson.D{{"useruuid", userId}}
	result := []*model.Withdrawal{}
	if err := utils.FindAll(ctx, db.DatabaseStore.GetDatabase(), constants.Collection_Withdrawal, filter, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (l LoanDaoImpl) CreateWithdrawal(ctx context.Context, withdrawal *model.Withdrawal) error {
	collection := l.Db.Collection(constants.Collection_Withdrawal)
	_, err := collection.InsertOne(ctx, withdrawal)
	if err != nil {
		return err
	}
	return nil
}

func (l LoanDaoImpl) GetUserByUUID(ctx context.Context, uuid string) (*uModel.User, error) {
	filter := bson.D{{"uuid", uuid}}
	user := &uModel.User{}
	if err := l.Db.Collection(constants.Collection_User).FindOne(ctx, filter).Decode(&user); err != nil {
		return nil, err
	}
	return user, nil
}

func (l LoanDaoImpl) GetBusinessOwner(ctx context.Context, uuid string) (*uModel.BusinessOwner, error) {
	filter := bson.D{{"uuid", uuid}}
	bo := &uModel.BusinessOwner{}
	if err := l.Db.Collection(constants.Collection_BusinessOwner).FindOne(ctx, filter).Decode(&bo); err != nil {
		return nil, err
	}
	return bo, nil
}

func (l LoanDaoImpl) CreateRepayment(ctx context.Context, repaymentSchedule *model.RepaymentSchedule) error {
	collection := db.DatabaseStore.GetDatabase().Collection(constants.Collection_Repayment)
	opts := options.Update()
	opts.SetUpsert(true)

	filter := bson.D{{Key: "uuid", Value: repaymentSchedule.UUID}}
	update := bson.D{{Key: "$set", Value: repaymentSchedule}}
	_, err := collection.UpdateOne(ctx, filter, update, opts)
	if err != nil {
		msg := "problem creating repaymentSchedule"
		return utils.WrapError(err, msg)
	}
	return nil
}

func (l LoanDaoImpl) GetRepayment(ctx context.Context, uuid string) (*model.RepaymentSchedule, error) {
	filter := bson.D{{"uuid", uuid}}
	loan := &model.RepaymentSchedule{}
	if err := l.Db.Collection(constants.Collection_Repayment).FindOne(ctx, filter).Decode(&loan); err != nil {
		return nil, err
	}
	return loan, nil
}

func (l LoanDaoImpl) GetDeposits(ctx context.Context, userId string) ([]*model.Deposit, error) {
	filter := bson.D{{"useruuid", userId}}
	result := []*model.Deposit{}
	if err := utils.FindAll(ctx, db.DatabaseStore.GetDatabase(), constants.Collection_Deposit, filter, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (l LoanDaoImpl) CreateDeposit(ctx context.Context, deposit *model.Deposit) error {
	collection := l.Db.Collection(constants.Collection_Deposit)
	_, err := collection.InsertOne(ctx, deposit)
	if err != nil {
		return err
	}
	return nil
}
