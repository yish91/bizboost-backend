package handler

import (
	"bizboost/internal/api/loan/model"
	"bizboost/internal/api/loan/service"
	"bizboost/internal/platform/utils"
	"github.com/go-chi/chi"
	"net/http"
)

type LoanHandler interface {
	CreateLoan(w http.ResponseWriter, req *http.Request)
	GetLoans(w http.ResponseWriter, req *http.Request)
	CloseLoan(w http.ResponseWriter, req *http.Request)
	CreateBid(w http.ResponseWriter, req *http.Request)
	GetBids(w http.ResponseWriter, req *http.Request)
	RepayLoan(w http.ResponseWriter, req *http.Request)
	GetPortfolio(w http.ResponseWriter, req *http.Request)
	GetWithdrawals(w http.ResponseWriter, req *http.Request)
	CreateWithdrawal(w http.ResponseWriter, req *http.Request)
	GetDeposits(w http.ResponseWriter, req *http.Request)
	CreateDeposit(w http.ResponseWriter, req *http.Request)
	GetRepayments(w http.ResponseWriter, req *http.Request)
}

type LoanHandlerImpl struct {
	LoanSvc service.LoanSvc
}

func NewLoanHandlerImpl(loanSvc service.LoanSvc) LoanHandler {
	return LoanHandlerImpl{LoanSvc: loanSvc}
}

func (l LoanHandlerImpl) CreateLoan(w http.ResponseWriter, req *http.Request) {
	requestModel := &model.LoanRequest{}
	utils.RunWithValidationAndWithUser(w, req, requestModel, func(userId string) (response interface{}, err error) {
		return l.LoanSvc.CreateLoan(requestModel, userId)
	})
}

func (l LoanHandlerImpl) GetLoans(w http.ResponseWriter, req *http.Request) {
	utils.RunWithoutValidationWithUser(w, req, func(userId string) (response interface{}, err error) {
		return l.LoanSvc.GetLoans(userId)
	})
}

func (l LoanHandlerImpl) CloseLoan(w http.ResponseWriter, req *http.Request) {
	id := chi.URLParam(req, "id")
	utils.RunWithoutValidationWithUser(w, req, func(userId string) (response interface{}, err error) {
		return l.LoanSvc.CloseLoan(id)
	})
}

func (l LoanHandlerImpl) CreateBid(w http.ResponseWriter, req *http.Request) {
	requestModel := &model.BidRequest{}
	id := chi.URLParam(req, "id")
	utils.RunWithValidationAndWithUser(w, req, requestModel, func(userId string) (response interface{}, err error) {
		return l.LoanSvc.CreateBid(requestModel, id, userId)
	})
}

func (l LoanHandlerImpl) GetBids(w http.ResponseWriter, req *http.Request) {
	utils.RunWithoutValidationWithUser(w, req, func(userId string) (response interface{}, err error) {
		return l.LoanSvc.GetBids(userId)
	})
}

func (l LoanHandlerImpl) RepayLoan(w http.ResponseWriter, req *http.Request) {
	requestModel := &model.RepaymentRequest{}
	id := chi.URLParam(req, "id")
	utils.RunWithValidationWithoutUser(w, req, requestModel, func() (response interface{}, err error) {
		return nil, l.LoanSvc.RepayLoan(requestModel, id)
	})
}

func (l LoanHandlerImpl) GetPortfolio(w http.ResponseWriter, req *http.Request) {
	utils.RunWithoutValidationWithUser(w, req, func(userId string) (response interface{}, err error) {
		return l.LoanSvc.GetPortfolio(userId)
	})
}

func (l LoanHandlerImpl) GetWithdrawals(w http.ResponseWriter, req *http.Request) {
	utils.RunWithoutValidationWithUser(w, req, func(userId string) (response interface{}, err error) {
		return l.LoanSvc.GetWithdrawals(userId)
	})
}

func (l LoanHandlerImpl) CreateWithdrawal(w http.ResponseWriter, req *http.Request) {
	requestModel := &model.WithdrawalRequest{}
	utils.RunWithValidationAndWithUser(w, req, requestModel, func(userId string) (response interface{}, err error) {
		return nil, l.LoanSvc.CreateWithdrawal(requestModel, userId)
	})
}

func (l LoanHandlerImpl) GetDeposits(w http.ResponseWriter, req *http.Request) {
	utils.RunWithoutValidationWithUser(w, req, func(userId string) (response interface{}, err error) {
		return l.LoanSvc.GetDeposits(userId)
	})
}

func (l LoanHandlerImpl) CreateDeposit(w http.ResponseWriter, req *http.Request) {
	requestModel := &model.DepositRequest{}
	utils.RunWithValidationAndWithUser(w, req, requestModel, func(userId string) (response interface{}, err error) {
		return nil, l.LoanSvc.CreateDeposit(requestModel, userId)
	})
}

func (l LoanHandlerImpl) GetRepayments(w http.ResponseWriter, req *http.Request) {
	utils.RunWithoutValidationWithUser(w, req, func(userId string) (response interface{}, err error) {
		return l.LoanSvc.GetRepayments(userId)
	})
}
