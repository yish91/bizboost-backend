package model

import (
	userModel "bizboost/internal/api/user/model"
	"bizboost/internal/platform/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type LoanRequest struct {
	Details       LoanDetails   `bson:",omitempty" json:"details,omitempty"`
	BiddingWindow BiddingWindow `bson:",omitempty" json:"biddingWindow,omitempty"`
}

type Loan struct {
	Id            primitive.ObjectID `bson:"_id,omitempty" json:"-"`
	UUID          string             `bson:",omitempty" json:"uuid,omitempty"`
	BusinessOwner string             `bson:",omitempty" json:"businessOwnerUUID,omitempty"`
	Details       LoanDetails        `bson:",omitempty" json:"details,omitempty"`
	BiddingWindow BiddingWindow      `bson:",omitempty" json:"biddingWindow,omitempty"`
	Status        string             `bson:",omitempty" json:"status,omitempty"`
	Audit         *utils.Audit       `bson:",omitempty" json:"audit,omitempty"`
}

type LoanResponse struct {
	Loan
	BusinessOwner *userModel.BusinessOwner `json:"businessOwner,omitempty"`
	User          *userModel.User          `json:"user,omitempty"`
}

type LoanDetails struct {
	Purpose string `bson:"" json:"purpose" validate:"required"`
	LoanRepayment
}

type LoanRepayment struct {
	Amount       float64 `bson:",omitempty" json:"amount" validate:"required"`
	InterestRate float64 `bson:",omitempty" json:"interestRate" validate:"required"`
	TimePeriod   string  `bson:"" json:"timePeriod" validate:"required"`
	Frequency    int     `bson:"" json:"frequency" validate:"required"`
}

type BiddingWindow struct {
	StartTime time.Time `bson:"" json:"startTime" validate:"required"`
	EndTime   time.Time `bson:"" json:"endTime" validate:"required"`
}

type LoanStatus int

const (
	LOAN_STATUS_ACTIVE LoanStatus = iota
	LOAN_STATUS_CLOSED
)

func (s LoanStatus) String() string {
	return [...]string{"Active", "Closed"}[s]
}

type BidRequest struct {
	Amount       float64 `bson:",omitempty" json:"amount" validate:"required"`
	InterestRate float64 `bson:",omitempty" json:"interestRate" validate:"required"`
}

type Bid struct {
	Id           primitive.ObjectID `bson:"_id,omitempty" json:"-"`
	UUID         string             `bson:",omitempty" json:"uuid,omitempty"`
	UserUUID     string             `bson:",omitempty" json:"userUUID,omitempty"`
	LoanUUID     string             `bson:",omitempty" json:"loanUUID,omitempty"`
	Amount       float64            `bson:",omitempty" json:"amount" validate:"required"`
	InterestRate float64            `bson:",omitempty" json:"interestRate" validate:"required"`
	Status       string             `bson:",omitempty" json:"status,omitempty"`
	Repayments   []*Repayment       `bson:",omitempty" json:"repayments"`
	Audit        *utils.Audit       `bson:",omitempty" json:"audit,omitempty"`
}

type BidResponse struct {
	Bid
	Loan          *Loan                    `json:"loan,omitempty"`
	BusinessOwner *userModel.BusinessOwner `json:"businessOwner,omitempty"`
	User          *userModel.User          `json:"user,omitempty"`
}

type Repayment struct {
	UUID          string    `bson:",omitempty" json:"uuid,omitempty"`
	Amount        float64   `bson:",omitempty" json:"amount"`
	Paid          float64   `bson:",omitempty" json:"paid"`
	RepaymentDate time.Time `bson:",omitempty" json:"repaymentDate"`
	Status        string    `bson:",omitempty" json:"status"`
}

type RepaymentCalc struct {
	UUID   string  `bson:",omitempty" json:"uuid,omitempty"`
	Amount float64 `bson:",omitempty" json:"amount"`
}

type RepaymentRequest struct {
	Amount float64 `bson:",omitempty" json:"amount"`
}

type RepaymentSchedule struct {
	Id         primitive.ObjectID `bson:"_id,omitempty" json:"-"`
	UUID       string             `bson:",omitempty" json:"uuid,omitempty"`
	Repayments []*Repayment       `bson:",omitempty" json:"repayments,omitempty"`
	Status     string             `bson:",omitempty" json:"status,omitempty"`
}

type RepaymentResponse struct {
	RepaymentSchedule
	Loan *Loan `bson:",omitempty" json:"loan" validate:"required"`
}

type Portfolio struct {
	Bid           *Bid                     `bson:",omitempty" json:"bid" validate:"required"`
	Loan          *Loan                    `bson:",omitempty" json:"loan" validate:"required"`
	BusinessOwner *userModel.BusinessOwner `json:"businessOwner,omitempty"`
	User          *userModel.User          `json:"user,omitempty"`
}

type Withdrawal struct {
	Id             primitive.ObjectID `bson:"_id,omitempty" json:"-"`
	UUID           string             `bson:",omitempty" json:"uuid" validate:"required"`
	UserUUID       string             `bson:",omitempty" json:"userUUID,omitempty"`
	Amount         float64            `bson:",omitempty" json:"amount" validate:"required"`
	Status         string             `bson:",omitempty" json:"status,omitempty"`
	Type           string             `bson:",omitempty" json:"type,omitempty"`
	WithdrawalDate time.Time          `bson:",omitempty" json:"withdrawalDate" validate:"required"`
	TxUUID         string             `bson:",omitempty" json:"txUUID,omitempty"`
}

type WithdrawalRequest struct {
	Amount float64 `bson:",omitempty" json:"amount" validate:"required"`
}

type RepaymentStatus int

const (
	REPAYMENT_STATUS_PENDING RepaymentStatus = iota
	REPAYMENT_STATUS_PAID
	REPAYMENT_STATUS_PARTIAL
)

func (s RepaymentStatus) String() string {
	return [...]string{"Pending", "Paid", "Partial"}[s]
}

type BidStatus int

const (
	BID_STATUS_ACTIVE BidStatus = iota
	BID_STATUS_CLOSED
	BID_STATUS_REJECTED
)

func (s BidStatus) String() string {
	return [...]string{"Active", "Closed", "Rejected"}[s]
}

type WithdrawalStatus int

const (
	WITHDRAWAL_STATUS_PENDING WithdrawalStatus = iota
	WITHDRAWAL_STATUS_SUCCESSFUL
)

func (s WithdrawalStatus) String() string {
	return [...]string{"Pending", "Successful"}[s]
}

type WithdrawalType int

const (
	WITHDRAWAL_TYPE_USER WithdrawalType = iota
	WITHDRAWAL_TYPE_BID
)

func (s WithdrawalType) String() string {
	return [...]string{"User", "Bid"}[s]
}

type Deposit struct {
	Id          primitive.ObjectID `bson:"_id,omitempty" json:"-"`
	UUID        string             `bson:",omitempty" json:"uuid" validate:"required"`
	UserUUID    string             `bson:",omitempty" json:"userUUID,omitempty"`
	Amount      float64            `bson:",omitempty" json:"amount" validate:"required"`
	Status      string             `bson:",omitempty" json:"status,omitempty"`
	Type        string             `bson:",omitempty" json:"type,omitempty"`
	DepositDate time.Time          `bson:",omitempty" json:"depositDate" validate:"required"`
	TxUUID      string             `bson:",omitempty" json:"txUUID,omitempty"`
}

type DepositRequest struct {
	Amount float64 `bson:",omitempty" json:"amount" validate:"required"`
}

type DepositStatus int

const (
	DEPOSIT_STATUS_PENDING DepositStatus = iota
	DEPOSIT_STATUS_SUCCESSFUL
)

func (s DepositStatus) String() string {
	return [...]string{"Pending", "Successful"}[s]
}

type DepositType int

const (
	DEPOSIT_TYPE_USER DepositType = iota
	DEPOSIT_TYPE_REPAYMENT
)

func (s DepositType) String() string {
	return [...]string{"User", "Repayment"}[s]
}
