package router

import (
	"bizboost/internal/api/loan/db"
	"bizboost/internal/api/loan/handler"
	"bizboost/internal/api/loan/service"
	"bizboost/internal/platform/authentication"

	platformDb "bizboost/internal/platform/db"

	"github.com/go-chi/chi"
)

func SetLoanRoutes(router *chi.Mux) *chi.Mux {

	h := handler.NewLoanHandlerImpl(service.NewLoanSvcImpl(db.NewLoanDaoImpl(platformDb.DatabaseStore.GetDatabase())))

	router.Group(func(r chi.Router) {
		// validating JWT token
		r.Use(authentication.RequireTokenAuthentication)

		r.Post("/loan", h.CreateLoan)
		r.Get("/loan", h.GetLoans)
		r.Post("/loan/{id}/bid", h.CreateBid)
		r.Get("/bid", h.GetBids)
		r.Post("/loan/{id}/close", h.CloseLoan)
		r.Post("/loan/{id}/repay", h.RepayLoan)
		r.Get("/portfolio", h.GetPortfolio)
		r.Get("/withdrawal", h.GetWithdrawals)
		r.Post("/withdrawal", h.CreateWithdrawal)
		r.Get("/deposit", h.GetDeposits)
		r.Post("/deposit", h.CreateDeposit)
		r.Get("/repayment", h.GetRepayments)
	})

	return router
}
