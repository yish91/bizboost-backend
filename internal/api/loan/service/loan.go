package service

import (
	"bizboost/internal/api/loan/db"
	"bizboost/internal/api/loan/model"
	userModel "bizboost/internal/api/user/model"
	"bizboost/internal/platform/utils"
	"context"
	"errors"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/mongo"
	"math"
	"strconv"
	"time"
)

type LoanSvc interface {
	CreateLoan(loanRequest *model.LoanRequest, userId string) (*model.Loan, error)
	GetLoans(userId string) ([]*model.LoanResponse, error)
	CreateBid(bidRequest *model.BidRequest, loanId string, userId string) (*model.Bid, error)
	GetBids(userId string) ([]*model.BidResponse, error)
	RepayLoan(request *model.RepaymentRequest, loanId string) error
	GetPortfolio(userId string) ([]*model.Portfolio, error)
	GetWithdrawals(userId string) ([]*model.Withdrawal, error)
	CreateWithdrawal(request *model.WithdrawalRequest, userId string) error
	CloseLoan(loanId string) (*model.RepaymentSchedule, error)
	GetRepayments(userId string) ([]*model.RepaymentResponse, error)
	GetDeposits(userId string) ([]*model.Deposit, error)
	CreateDeposit(request *model.DepositRequest, userId string) error
}

type LoanSvcImpl struct {
	loanDao db.LoanDao
}

func NewLoanSvcImpl(loanDao db.LoanDao) LoanSvc {
	return LoanSvcImpl{
		loanDao: loanDao,
	}
}

func (l LoanSvcImpl) CreateLoan(loanRequest *model.LoanRequest, userId string) (*model.Loan, error) {
	loan := l.loanRequestToLoan(loanRequest, userId)
	txnErr := utils.RunInTxn(func(sessionContext mongo.SessionContext) error {
		err := l.loanDao.CreateLoan(sessionContext, loan)
		if err != nil {
			return err
		}
		return nil
	})
	if txnErr != nil {
		return nil, txnErr
	}
	return loan, nil
}

func (l LoanSvcImpl) loanRequestToLoan(loanRequest *model.LoanRequest, userId string) *model.Loan {
	loan := new(model.Loan)
	loan.BusinessOwner = userId
	loan.Details = loanRequest.Details
	loan.BiddingWindow = loanRequest.BiddingWindow
	loan.UUID = uuid.New().String()
	loan.Status = model.LOAN_STATUS_ACTIVE.String()
	loan.Audit = utils.NewAuditWithoutEvent(userId)
	return loan
}

func (l LoanSvcImpl) GetLoans(userId string) ([]*model.LoanResponse, error) {
	var loans []*model.Loan
	user, err := l.loanDao.GetUserByUUID(context.TODO(), userId)
	if err != nil {
		return nil, err
	}
	if user.Type == userModel.USER_TYPE_INVESTOR.String() {
		loans, err = l.loanDao.GetLoans(context.TODO())
		if err != nil {
			return nil, err
		}
	} else {
		loans, err = l.loanDao.GetLoansForBusinessOwner(context.TODO(), userId)
		if err != nil {
			return nil, err
		}
	}

	var result []*model.LoanResponse
	for _, loan := range loans {
		bo, err := l.loanDao.GetBusinessOwner(context.TODO(), loan.BusinessOwner)
		if err == nil {
			user, err := l.loanDao.GetUserByUUID(context.TODO(), loan.BusinessOwner)
			if err == nil {
				loanResponse := new(model.LoanResponse)
				loanResponse.Loan = *loan
				loanResponse.BusinessOwner = bo
				loanResponse.User = user
				result = append(result, loanResponse)
			}
		}
	}
	return result, nil
}

func (l LoanSvcImpl) GetRepayments(userId string) ([]*model.RepaymentResponse, error) {
	var loans []*model.Loan
	user, err := l.loanDao.GetUserByUUID(context.TODO(), userId)
	if err != nil {
		return nil, err
	}
	if user.Type != userModel.USER_TYPE_BO.String() {
		return nil, errors.New("Not a business owner")
	} else {
		loans, err = l.loanDao.GetClosedLoansForBusinessOwner(context.TODO(), userId)
		if err != nil {
			return nil, err
		}
	}

	var repayments []*model.RepaymentResponse
	for _, loan := range loans {
		r, err := l.loanDao.GetRepayment(context.TODO(), loan.UUID)
		if err == nil {
			repaymentResponse := new(model.RepaymentResponse)
			repaymentResponse.RepaymentSchedule = *r
			repaymentResponse.Loan = loan
			repayments = append(repayments, repaymentResponse)
		}
	}
	return repayments, nil
}

func (l LoanSvcImpl) CreateBid(bidRequest *model.BidRequest, loanId string, userId string) (*model.Bid, error) {
	bid, err := l.bidRequestToBid(bidRequest, loanId, userId)
	if err != nil {
		return nil, err
	}
	txnErr := utils.RunInTxn(func(sessionContext mongo.SessionContext) error {
		err := l.loanDao.CreateBid(sessionContext, bid)
		if err != nil {
			return err
		}
		return nil
	})
	if txnErr != nil {
		return nil, txnErr
	}
	return bid, nil
}

func (l LoanSvcImpl) bidRequestToBid(bidRequest *model.BidRequest, loanId string, userId string) (*model.Bid, error) {
	dbLoan, err := l.loanDao.GetLoan(context.TODO(), loanId)
	if err != nil {
		return nil, err
	}
	if dbLoan.Details.Amount < bidRequest.Amount {
		bidRequest.Amount = dbLoan.Details.Amount
	}
	bidRequest.InterestRate /= 100
	bid := new(model.Bid)
	bid.Amount = bidRequest.Amount
	bid.InterestRate = bidRequest.InterestRate
	bid.UserUUID = userId
	bid.LoanUUID = loanId
	bid.Status = model.BID_STATUS_ACTIVE.String()
	bid.Audit = utils.NewAuditWithoutEvent(userId)
	bid.UUID = uuid.New().String()
	return bid, nil
}

func (l LoanSvcImpl) GetBids(userId string) ([]*model.BidResponse, error) {
	bids, err := l.loanDao.GetBids(context.TODO(), userId)
	if err != nil {
		return nil, err
	}
	var result []*model.BidResponse
	for _, bid := range bids {
		loan, err := l.loanDao.GetLoan(context.TODO(), bid.LoanUUID)
		if err == nil {
			bo, err := l.loanDao.GetBusinessOwner(context.TODO(), loan.BusinessOwner)
			if err == nil {
				user, err := l.loanDao.GetUserByUUID(context.TODO(), loan.BusinessOwner)
				if err == nil {
					bidResponse := new(model.BidResponse)
					bidResponse.Loan = loan
					bidResponse.Bid = *bid
					bidResponse.BusinessOwner = bo
					bidResponse.User = user
					result = append(result, bidResponse)
				}
			}
		}
	}
	return result, nil
}

func (l LoanSvcImpl) RepayLoan(request *model.RepaymentRequest, loanId string) error {
	dbBids, err := l.loanDao.GetClosedBidsForLoans(context.TODO(), loanId)
	if err != nil {
		return err
	}

	var repaymentIds []string
	repaymentCalcMap := make(map[string]*model.RepaymentCalc)
	repaymentMap := make(map[string]*model.Repayment)

	repaymentSchedule, err := l.loanDao.GetRepayment(context.TODO(), loanId)
	if err != nil {
		return err
	}

	counter := 0
	for _, r := range repaymentSchedule.Repayments {
		if r.Status != model.REPAYMENT_STATUS_PAID.String() {
			diff := r.Amount - r.Paid
			if diff <= request.Amount {
				request.Amount = math.Floor((request.Amount-diff)*100) / 100
				r.Paid = r.Amount
				repaymentIds = append(repaymentIds, r.UUID)
				repaymentCalc := new(model.RepaymentCalc)
				repaymentCalc.UUID = r.UUID
				repaymentCalc.Amount = math.Floor(diff*100) / 100
				repaymentCalcMap[r.UUID] = repaymentCalc
			} else if request.Amount > 0 {
				r.Paid += request.Amount
				repaymentCalc := new(model.RepaymentCalc)
				repaymentCalc.UUID = r.UUID
				repaymentCalc.Amount = math.Floor(request.Amount*100) / 100
				repaymentCalcMap[r.UUID] = repaymentCalc
				request.Amount = 0
				repaymentIds = append(repaymentIds, r.UUID)
			}

			if r.Amount == r.Paid {
				r.Status = model.REPAYMENT_STATUS_PAID.String()
				counter += 1
			} else if r.Paid > 0 {
				r.Status = model.REPAYMENT_STATUS_PARTIAL.String()
			}
			repaymentMap[r.UUID] = r

		} else {
			counter += 1
		}
	}
	if counter == len(repaymentSchedule.Repayments) {
		repaymentSchedule.Status = model.REPAYMENT_STATUS_PAID.String()
	} else {
		repaymentSchedule.Status = model.REPAYMENT_STATUS_PARTIAL.String()
	}
	_ = l.loanDao.CreateRepayment(context.TODO(), repaymentSchedule)

	for _, repaymentId := range repaymentIds {
		repayment := repaymentMap[repaymentId]
		repaymentCalc := repaymentCalcMap[repaymentId]
		for _, bid := range dbBids {
			for _, r := range bid.Repayments {
				if r.UUID == repaymentId {
					r.Status = repayment.Status
					if repayment.Status == model.REPAYMENT_STATUS_PAID.String() {
						diff := math.Floor((r.Amount-r.Paid)*100) / 100
						r.Paid = r.Amount
						_ = l.CreateBidDeposit(diff, bid.UUID, bid.UserUUID)
					} else {
						proportion := r.Amount / repayment.Amount
						rPaid := math.Floor(repaymentCalc.Amount*proportion*100) / 100
						_ = l.CreateBidDeposit(rPaid, bid.UUID, bid.UserUUID)
						r.Paid += rPaid
					}
				}
			}
			_ = l.loanDao.CreateBid(context.TODO(), bid)
		}
	}
	return nil

}

func (l LoanSvcImpl) repaymentRequestToRepayment(repaymentRequest *model.RepaymentRequest) *model.Repayment {
	repayment := new(model.Repayment)
	repayment.UUID = uuid.New().String()
	repayment.Amount = repaymentRequest.Amount
	repayment.RepaymentDate = time.Now()
	return repayment
}

func (l LoanSvcImpl) GetPortfolio(userId string) ([]*model.Portfolio, error) {
	closedBids, err := l.loanDao.GetClosedBids(context.TODO(), userId)
	if err != nil {
		return nil, err
	}
	var portfolios []*model.Portfolio
	for _, v := range closedBids {
		loanId := v.LoanUUID
		loan, err := l.loanDao.GetLoan(context.TODO(), loanId)
		if err == nil {
			if err == nil {
				bo, err := l.loanDao.GetBusinessOwner(context.TODO(), loan.BusinessOwner)
				if err == nil {
					user, err := l.loanDao.GetUserByUUID(context.TODO(), loan.BusinessOwner)
					if err == nil {
						portfolio := new(model.Portfolio)
						portfolio.Bid = v
						portfolio.Loan = loan
						portfolio.User = user
						portfolio.BusinessOwner = bo
						portfolios = append(portfolios, portfolio)
					}
				}
			}
		}
	}
	return portfolios, nil
}

func (l LoanSvcImpl) GetWithdrawals(userId string) ([]*model.Withdrawal, error) {
	return l.loanDao.GetWithdrawals(context.TODO(), userId)
}

func (l LoanSvcImpl) GetDeposits(userId string) ([]*model.Deposit, error) {
	return l.loanDao.GetDeposits(context.TODO(), userId)
}

func (l LoanSvcImpl) CreateWithdrawal(request *model.WithdrawalRequest, userId string) error {
	deposits, err := l.GetDeposits(userId)
	if err != nil {
		return err
	}
	totalDeposits := 0.0
	for _, deposit := range deposits {
		if deposit.Status == model.DEPOSIT_STATUS_SUCCESSFUL.String() {
			totalDeposits += deposit.Amount
		}
	}
	withdrawals, err := l.GetWithdrawals(userId)
	totalWithdrawals := 0.0
	if err == nil {
		for _, w := range withdrawals {
			totalWithdrawals += w.Amount
		}
	}
	totalWithdrawals += request.Amount
	if totalDeposits < totalWithdrawals {
		return utils.LogError(errors.New("insufficient wallet amt"))
	}
	withdrawal := new(model.Withdrawal)
	withdrawal.UUID = uuid.New().String()
	withdrawal.UserUUID = userId
	withdrawal.Amount = request.Amount
	withdrawal.WithdrawalDate = time.Now()
	withdrawal.Status = model.WITHDRAWAL_STATUS_SUCCESSFUL.String()
	withdrawal.Type = model.WITHDRAWAL_TYPE_USER.String()
	return l.loanDao.CreateWithdrawal(context.TODO(), withdrawal)
}

func (l LoanSvcImpl) CreateDeposit(request *model.DepositRequest, userId string) error {
	if request.Amount <= 0 {
		return errors.New("deposit must be positive")
	}

	deposit := new(model.Deposit)
	deposit.UUID = uuid.New().String()
	deposit.UserUUID = userId
	deposit.Amount = request.Amount
	deposit.DepositDate = time.Now()
	deposit.Status = model.DEPOSIT_STATUS_SUCCESSFUL.String()
	deposit.Type = model.DEPOSIT_TYPE_USER.String()
	return l.loanDao.CreateDeposit(context.TODO(), deposit)
}

func (l LoanSvcImpl) CloseLoan(loanId string) (*model.RepaymentSchedule, error) {
	loan, err := l.loanDao.GetLoan(context.TODO(), loanId)
	if err == nil {
		if loan.Status == model.LOAN_STATUS_CLOSED.String() {
			return nil, errors.New("LOAN IS CLOSED")
		}
		bids, err := l.loanDao.GetBidsForLoans(context.TODO(), loanId)
		if err == nil {
			accepted, rejected := l.SortBidsForCloseLoan(loan, bids)
			for _, bid := range rejected {
				_ = l.loanDao.CreateBid(context.TODO(), bid)
			}

			amount := 0.0
			interest := 0.0

			for _, bid := range accepted {
				amount += bid.Amount
				interest += bid.Amount * bid.InterestRate

				var repayments []*model.Repayment

				repaymentInterest := (bid.Amount * bid.InterestRate) / float64(loan.Details.Frequency)
				repaymentInterest = math.Floor(repaymentInterest*100) / 100

				for i := 1; i <= loan.Details.Frequency; i++ {
					repayment := new(model.Repayment)
					timePeriod := time.Now()
					if loan.Details.TimePeriod == "Annual" {
						timePeriod = loan.BiddingWindow.EndTime.AddDate(i, 0, 0)

					} else {
						timePeriod = loan.BiddingWindow.EndTime.AddDate(0, i, 0)
					}
					repaymentAmount := repaymentInterest

					if i == loan.Details.Frequency {
						repaymentAmount += bid.Amount
					}
					repayment.UUID = strconv.Itoa(i) + "|" + loan.UUID
					repayment.Status = model.REPAYMENT_STATUS_PENDING.String()
					repayment.RepaymentDate = timePeriod
					repayment.Amount = repaymentAmount
					repayments = append(repayments, repayment)
				}
				bid.Repayments = repayments
				_ = l.loanDao.CreateBid(context.TODO(), bid)
				_ = l.CreateBidWithdrawal(bid.Amount, bid.UUID, bid.UserUUID)
			}

			repaymentSchedule := new(model.RepaymentSchedule)
			var repayments []*model.Repayment
			repaymentSchedule.UUID = loan.UUID

			repaymentInterest := interest / float64(loan.Details.Frequency)
			repaymentInterest = math.Floor(repaymentInterest*100) / 100

			for i := 1; i <= loan.Details.Frequency; i++ {
				repayment := new(model.Repayment)
				timePeriod := time.Now()
				if loan.Details.TimePeriod == "Annual" {
					timePeriod = loan.BiddingWindow.EndTime.AddDate(i, 0, 0)

				} else {
					timePeriod = loan.BiddingWindow.EndTime.AddDate(0, i, 0)
				}
				repaymentAmount := repaymentInterest

				if i == loan.Details.Frequency {
					repaymentAmount += amount
				}
				repayment.UUID = strconv.Itoa(i) + "|" + loan.UUID
				repayment.Status = model.REPAYMENT_STATUS_PENDING.String()
				repayment.RepaymentDate = timePeriod
				repayment.Amount = repaymentAmount
				repayments = append(repayments, repayment)
			}
			repaymentSchedule.Repayments = repayments
			repaymentSchedule.Status = model.REPAYMENT_STATUS_PENDING.String()
			err = l.loanDao.CreateRepayment(context.TODO(), repaymentSchedule)

			if err != nil {
				return nil, err
			}

			loan.Status = model.LOAN_STATUS_CLOSED.String()
			err = l.loanDao.UpdateLoan(context.TODO(), loan)
			if err != nil {
				return nil, err
			}
			return repaymentSchedule, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}

func (l LoanSvcImpl) SortBidsForCloseLoan(loan *model.Loan, bids []*model.Bid) ([]*model.Bid, []*model.Bid) {
	interestRate := loan.Details.InterestRate
	amount := loan.Details.Amount
	interest := loan.Details.Amount * loan.Details.InterestRate
	presentAmount := 0.0
	presentInterest := 0.0
	var acceptedBids []*model.Bid
	var rejectedBids []*model.Bid
	for _, bid := range bids {
		futureAmount := presentAmount + bid.Amount
		wallet, err := l.GetWallet(bid.UserUUID)
		if err != nil {
			bid.Status = model.BID_STATUS_REJECTED.String()
			rejectedBids = append(rejectedBids, bid)
		} else {
			if bid.InterestRate <= interestRate {
				if futureAmount <= amount && bid.Amount <= wallet {
					presentAmount += bid.Amount
					presentInterest += bid.Amount * bid.InterestRate
					bid.Status = model.BID_STATUS_CLOSED.String()
					acceptedBids = append(acceptedBids, bid)
				} else {
					diff := amount - presentAmount
					if diff > 0 && diff <= wallet {
						bid.Amount = diff
						presentAmount += bid.Amount
						presentInterest += bid.Amount * bid.InterestRate
						bid.Status = model.BID_STATUS_CLOSED.String()
						acceptedBids = append(acceptedBids, bid)
					} else {
						bid.Status = model.BID_STATUS_REJECTED.String()
						rejectedBids = append(rejectedBids, bid)
					}
				}
			}
		}
	}

	if presentAmount != amount {
		for _, bid := range bids {
			if bid.Status == model.BID_STATUS_ACTIVE.String() {
				wallet, err := l.GetWallet(bid.UserUUID)
				if err != nil {
					bid.Status = model.BID_STATUS_REJECTED.String()
					rejectedBids = append(rejectedBids, bid)
				} else {
					bidInterest := bid.Amount * bid.InterestRate
					futureInterest := presentInterest + bidInterest
					futureAmount := presentAmount + bid.Amount
					if futureInterest < interest && futureAmount >= amount {
						diff := amount - presentAmount
						if diff <= wallet {
							bid.Amount = diff
							presentAmount += bid.Amount
							presentInterest += bid.Amount * bid.InterestRate
							bid.Status = model.BID_STATUS_CLOSED.String()
							acceptedBids = append(acceptedBids, bid)
						} else {
							bid.Status = model.BID_STATUS_REJECTED.String()
							rejectedBids = append(rejectedBids, bid)
						}
					} else {
						bid.Status = model.BID_STATUS_REJECTED.String()
						rejectedBids = append(rejectedBids, bid)
					}
				}
			}
		}
	}

	for _, bid := range bids {
		if bid.Status == model.BID_STATUS_ACTIVE.String() {
			bid.Status = model.BID_STATUS_REJECTED.String()
			rejectedBids = append(rejectedBids, bid)
		}
	}

	return acceptedBids, rejectedBids
}

func (l LoanSvcImpl) GetWallet(userId string) (float64, error) {
	deposits, err := l.GetDeposits(userId)
	if err != nil {
		return 0.0, err
	}
	withdrawals, err := l.GetWithdrawals(userId)
	if err != nil {
		return 0.0, err
	}

	totalDeposits := 0.0
	for _, deposit := range deposits {
		if deposit.Status == model.DEPOSIT_STATUS_SUCCESSFUL.String() {
			totalDeposits += deposit.Amount
		}
	}

	totalWithdrawals := 0.0
	for _, withdrawal := range withdrawals {
		totalWithdrawals += withdrawal.Amount
	}
	return totalDeposits - totalWithdrawals, nil
}

func (l LoanSvcImpl) CreateBidWithdrawal(amount float64, txUUID string, userId string) error {
	withdrawal := new(model.Withdrawal)
	withdrawal.UUID = uuid.New().String()
	withdrawal.UserUUID = userId
	withdrawal.Amount = amount
	withdrawal.WithdrawalDate = time.Now()
	withdrawal.Status = model.WITHDRAWAL_STATUS_SUCCESSFUL.String()
	withdrawal.Type = model.WITHDRAWAL_TYPE_BID.String()
	withdrawal.TxUUID = txUUID
	return l.loanDao.CreateWithdrawal(context.TODO(), withdrawal)
}

func (l LoanSvcImpl) CreateBidDeposit(amount float64, txUUID string, userId string) error {
	deposit := new(model.Deposit)
	deposit.UUID = uuid.New().String()
	deposit.UserUUID = userId
	deposit.Amount = amount
	deposit.DepositDate = time.Now()
	deposit.Status = model.DEPOSIT_STATUS_SUCCESSFUL.String()
	deposit.Type = model.DEPOSIT_TYPE_REPAYMENT.String()
	deposit.TxUUID = txUUID
	return l.loanDao.CreateDeposit(context.TODO(), deposit)
}
