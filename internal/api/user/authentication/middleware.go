package authentication

import (
	"bizboost/internal/api/user/model"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"errors"
	"net/http"
	"time"
)

type Payload struct {
	TimeStamp time.Time `json:",omitempty"`
}

func GetServiceCaller(r *http.Request) (string, error) {
	ciphertext := r.Header.Get("cipher")

	cipherByte, err := hex.DecodeString(ciphertext)
	if err != nil {
		return "", err
	}

	userServiceTypes := model.GetUserServiceTypes()

	for _, userServiceType := range userServiceTypes {
		plaintext, err := decrypt(userServiceType, cipherByte)
		if err == nil {
			payload := &Payload{}
			err = json.Unmarshal(plaintext, payload)
			if err == nil {
				now := time.Now().UTC()
				duration := 1 * time.Minute
				expiredTime := payload.TimeStamp.Add(duration)
				if now.After(expiredTime) {
					return "", errors.New("expired")
				}
				return userServiceType, nil
			}
		}
	}

	return "", errors.New("unable to find service caller")
}

func decrypt(passphrase string, data []byte) ([]byte, error) {
	key := []byte(createHash(passphrase))
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	nonceSize := gcm.NonceSize()
	nonce, ciphertext := data[:nonceSize], data[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return nil, err
	}
	return plaintext, nil
}

func createHash(key string) string {
	hasher := md5.New()
	hasher.Write([]byte(key))
	return hex.EncodeToString(hasher.Sum(nil))
}
