package db

import (
	"bizboost/internal/api/user/model"
	"bizboost/internal/platform/constants"
	"bizboost/internal/platform/db"
	"context"

	"bizboost/internal/platform/utils"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type UserDao interface {
	CreateUser(ctx context.Context, user *model.User) error
	UpdateUser(ctx context.Context, user *model.User) error
	GetUserByUserTypeAndEmail(ctx context.Context, userType, email string) (*model.User, error)
	GetUserByEmail(ctx context.Context, email string) (*model.User, error)
	GetUserByUUID(ctx context.Context, uuid string) (*model.User, error)

	// Token
	PersistVerificationToken(ctx context.Context, token *model.VerificationToken) error
	GetActiveVerificationToken(ctx context.Context, tokenType string, token string) (result *model.VerificationToken, err error)
	GetVerificationToken(ctx context.Context, tokenType string, token string) (result *model.VerificationToken, err error)
	UpdateVerificationToken(ctx context.Context, token *model.VerificationToken) error

	//BO
	UpsertBusinessOwner(ctx context.Context, bo *model.BusinessOwner) error
	GetBusinessOwner(ctx context.Context, uuid string) (*model.BusinessOwner, error)
}

type UserDaoImpl struct {
	Db *mongo.Database
}

func NewUserDaoImpl(db *mongo.Database) UserDao {
	return UserDaoImpl{Db: db}
}

func (u UserDaoImpl) CreateUser(ctx context.Context, user *model.User) error {
	user.Email = utils.FormatEmail(user.Email)
	collection := u.Db.Collection(constants.Collection_User)
	_, err := collection.InsertOne(ctx, user)
	if err != nil {
		return err
	}
	return nil
}

func (u UserDaoImpl) UpdateUser(ctx context.Context, user *model.User) error {
	user.Email = utils.FormatEmail(user.Email)
	idDoc := bson.D{{"uuid", user.UUID}}
	_, err := u.Db.Collection(constants.Collection_User).UpdateOne(ctx, idDoc, bson.D{
		{"$set", user},
	})
	if err != nil {
		return err
	}
	return nil
}

func (u UserDaoImpl) GetUserByUserTypeAndEmail(ctx context.Context, userType, email string) (*model.User, error) {
	email = utils.FormatEmail(email)
	filter := bson.D{{"email", email}, {"type", userType}}
	result := &model.User{}
	if err := u.Db.Collection(constants.Collection_User).FindOne(ctx, filter).Decode(&result); err != nil {
		return nil, err
	}
	return result, nil
}

func (u UserDaoImpl) GetUserByEmail(ctx context.Context, email string) (*model.User, error) {
	email = utils.FormatEmail(email)
	filter := bson.D{{"email", email}}
	result := &model.User{}
	if err := u.Db.Collection(constants.Collection_User).FindOne(ctx, filter).Decode(&result); err != nil {
		return nil, err
	}
	return result, nil
}

func (u UserDaoImpl) GetUserByUUID(ctx context.Context, uuid string) (*model.User, error) {
	filter := bson.D{{"uuid", uuid}}
	user := &model.User{}
	if err := u.Db.Collection(constants.Collection_User).FindOne(ctx, filter).Decode(&user); err != nil {
		return nil, err
	}
	return user, nil
}

// Token

func (u UserDaoImpl) PersistVerificationToken(ctx context.Context, token *model.VerificationToken) error {
	opts := options.Update()
	opts.SetUpsert(true)
	filter := bson.D{{"uuid", token.UUID}, {"tokentype", token.TokenType}}
	update := bson.D{{"$set", token}}
	_, err := u.Db.Collection(constants.Collection_Token).UpdateOne(ctx, filter, update, opts)
	return err
}

func (u UserDaoImpl) GetActiveVerificationToken(ctx context.Context, tokenType string, token string) (result *model.VerificationToken, err error) {
	verificationToken := &model.VerificationToken{}
	filter := bson.D{
		{"tokentype", tokenType},
		{"token", token},
		{"status", model.VerificationTokenStatusActive}}

	//TODO: Might be worth changing this to Find() instead of FindOne()
	err = u.Db.Collection(constants.Collection_Token).FindOne(ctx, filter).Decode(verificationToken)
	if err != nil {
		return nil, err
	}
	return verificationToken, nil
}

func (u UserDaoImpl) GetVerificationToken(ctx context.Context, tokenType string, token string) (result *model.VerificationToken, err error) {
	verificationToken := &model.VerificationToken{}
	filter := bson.D{
		{"tokentype", tokenType},
		{"token", token}}

	err = u.Db.Collection(constants.Collection_Token).FindOne(ctx, filter).Decode(verificationToken)
	if err != nil {
		return nil, err
	}
	return verificationToken, nil
}

func (u UserDaoImpl) UpdateVerificationToken(ctx context.Context, token *model.VerificationToken) error {
	idDoc := bson.D{{"_id", token.Id}}
	_, err := u.Db.Collection(constants.Collection_Token).UpdateOne(ctx, idDoc, bson.D{
		{"$set", token},
	})
	if err != nil {
		return err
	}
	return nil
}

func (u UserDaoImpl) UpsertBusinessOwner(ctx context.Context, bo *model.BusinessOwner) error {
	collection := db.DatabaseStore.GetDatabase().Collection(constants.Collection_BusinessOwner)
	opts := options.Update()
	opts.SetUpsert(true)

	filter := bson.D{{Key: "uuid", Value: bo.UUID}}
	update := bson.D{{Key: "$set", Value: bo}}
	_, err := collection.UpdateOne(ctx, filter, update, opts)
	if err != nil {
		msg := "problem creating bo"
		return utils.WrapError(err, msg)
	}
	return nil
}

func (u UserDaoImpl) GetBusinessOwner(ctx context.Context, uuid string) (*model.BusinessOwner, error) {
	filter := bson.D{{"uuid", uuid}}
	bo := &model.BusinessOwner{}
	if err := u.Db.Collection(constants.Collection_BusinessOwner).FindOne(ctx, filter).Decode(&bo); err != nil {
		return nil, err
	}
	return bo, nil
}
