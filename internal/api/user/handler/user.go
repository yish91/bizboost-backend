package handler

import (
	"bizboost/internal/api/user/model"
	"bizboost/internal/api/user/service"
	"bizboost/internal/platform/utils"
	"github.com/go-chi/chi"
	"net/http"
)

type UserHandler interface {
	UserSignUp(w http.ResponseWriter, req *http.Request)
	UserSignIn(w http.ResponseWriter, req *http.Request)
	GetPasswordResetToken(w http.ResponseWriter, req *http.Request)
	ResetPassword(w http.ResponseWriter, req *http.Request)
	GetActivateAccountToken(w http.ResponseWriter, req *http.Request)
	ActivateAccount(w http.ResponseWriter, req *http.Request)
	// requires token
	UpdatePassword(w http.ResponseWriter, req *http.Request)

	UserServiceTest(w http.ResponseWriter, req *http.Request)

	UpsertBusinessOwner(w http.ResponseWriter, req *http.Request)
	UploadBusinessOwnerDocument(w http.ResponseWriter, req *http.Request)
	GetBusinessOwner(w http.ResponseWriter, req *http.Request)
}

type UserHandlerImpl struct {
	UserSvc service.UserSvc
}

func NewUserHandlerImpl(userSvc service.UserSvc) UserHandler {
	return UserHandlerImpl{UserSvc: userSvc}
}

func (u UserHandlerImpl) UserSignUp(w http.ResponseWriter, req *http.Request) {
	requestModel := &model.SignUpRequest{}
	utils.RunWithValidationWithoutUser(w, req, requestModel, func() (response interface{}, err error) {
		return u.UserSvc.UserSignUp(requestModel)
	})
}

func (u UserHandlerImpl) UserSignIn(w http.ResponseWriter, req *http.Request) {
	requestModel := &model.SignInRequest{}
	utils.RunWithValidationWithoutUser(w, req, requestModel, func() (response interface{}, err error) {
		return u.UserSvc.UserSignIn(requestModel)
	})
}

func (u UserHandlerImpl) GetPasswordResetToken(w http.ResponseWriter, req *http.Request) {
	requestModel := &model.EmailAndUserType{}
	utils.RunWithValidationWithoutUser(w, req, requestModel, func() (response interface{}, err error) {
		return u.UserSvc.GetPasswordResetToken(requestModel)
	})
}

func (u UserHandlerImpl) ResetPassword(w http.ResponseWriter, req *http.Request) {
	requestModel := &model.TokenBasedPasswordResetRequest{}
	utils.RunWithValidationWithoutUser(w, req, requestModel, func() (response interface{}, err error) {
		return u.UserSvc.ResetPassword(requestModel)
	})
}

func (u UserHandlerImpl) GetActivateAccountToken(w http.ResponseWriter, req *http.Request) {
	requestModel := &model.EmailAndUserType{}
	utils.RunWithValidationWithoutUser(w, req, requestModel, func() (response interface{}, err error) {
		return u.UserSvc.GetActivateAccountToken(requestModel)
	})
}

func (u UserHandlerImpl) ActivateAccount(w http.ResponseWriter, req *http.Request) {
	requestModel := &model.ActivateAccountRequest{}
	utils.RunWithValidationWithoutUser(w, req, requestModel, func() (response interface{}, err error) {
		return u.UserSvc.ActivateAccount(requestModel)
	})
}

func (u UserHandlerImpl) UpdatePassword(w http.ResponseWriter, req *http.Request) {
	requestModel := &model.UpdatePasswordRequest{}
	utils.RunWithValidationWithoutUser(w, req, requestModel, func() (response interface{}, err error) {
		return u.UserSvc.UpdatePassword(requestModel)
	})
}

func (u UserHandlerImpl) UserServiceTest(w http.ResponseWriter, req *http.Request) {
	utils.RunWithoutValidationWithoutUser(w, req, func() (response interface{}, err error) {
		return u.UserSvc.UserServiceTest(req)
	})
}

func (u UserHandlerImpl) UpsertBusinessOwner(w http.ResponseWriter, req *http.Request) {
	requestModel := &model.BusinessOwner{}
	utils.RunWithValidationAndWithUser(w, req, requestModel, func(userId string) (response interface{}, err error) {
		return nil, u.UserSvc.UpsertBusinessOwner(requestModel, userId)
	})
}

func (u UserHandlerImpl) GetBusinessOwner(w http.ResponseWriter, req *http.Request) {
	id := chi.URLParam(req, "id")
	utils.RunWithoutValidationWithoutUser(w, req, func() (response interface{}, err error) {
		return u.UserSvc.GetBusinessOwner(id)
	})
}

func (u UserHandlerImpl) UploadBusinessOwnerDocument(w http.ResponseWriter, req *http.Request) {
	boUUID := chi.URLParam(req, "id")
	err := req.ParseMultipartForm(10 << 20)
	if err != nil {
		msg := "error parsing file"
		newErr := utils.WrapError(err, msg)
		http.Error(w, newErr.Error(), http.StatusBadRequest)
		return
	}
	file, fileHeader, err := req.FormFile("file")
	if err != nil {
		msg := "error retrieving file"
		newErr := utils.WrapError(err, msg)
		http.Error(w, newErr.Error(), http.StatusBadRequest)
		return
	}
	utils.RunWithoutValidationWithUser(w, req, func(userId string) (response interface{}, err error) {
		return u.UserSvc.UploadBusinessOwnerDocument(req.Context(), file, fileHeader, boUUID, userId)
	})
}
