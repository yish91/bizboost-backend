package model

import (
	"bizboost/internal/platform/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type BusinessOwner struct {
	Id            primitive.ObjectID `bson:"_id,omitempty" json:"-"`
	UUID          string             `bson:",omitempty" json:"uuid" validate:"required"`
	Documents     string             `bson:",omitempty" json:"documents"`
	Description   string             `bson:",omitempty" json:"description"`
	ContactNumber int64              `bson:",omitempty" json:"contactNumber"`
	Sector        string             `bson:",omitempty" json:"sector"`
	Address
	Audit *utils.Audit `bson:",omitempty" json:"audit,omitempty"`
}
