package model

import "strconv"

type PhoneNumber struct {
	CountryCode string `bson:",omitempty" json:"countryCode,omitempty"`
	Number      int    `bson:",omitempty" json:"number,omitempty"`
}

type Address struct {
	Address1   string `bson:",omitempty" json:"address1" validate:"required"`
	Address2   string `bson:",omitempty" json:"address2"`
	PostalCode string `bson:",omitempty" json:"postalCode" validate:"required"`
	Country    string `bson:",omitempty" json:"country" validate:"required"`
}

type Identity struct {
	IdentityType   string `bson:",omitempty" json:"identityType" validate:"required"`
	IdentityNumber string `bson:",omitempty" json:"identityNumber" validate:"required"`
}

func (p PhoneNumber) ToString() string {
	return p.CountryCode + strconv.Itoa(p.Number)
}

func (a Address) ToString() string {
	result := a.Address1 + ", "
	if a.Address2 != "" {
		result = result + a.Address2 + ", "
	}
	result = result + a.PostalCode + ", " + a.Country
	return result
}
