package model

import (
	"bizboost/internal/platform/constants"
	"bizboost/internal/platform/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type User struct {
	Id             primitive.ObjectID `bson:"_id,omitempty" json:"-"`
	UUID           string             `bson:",omitempty" json:"uuid,omitempty"`
	Email          string             `bson:",omitempty" json:"email,omitempty"`
	HashedPassword string             `bson:",omitempty" json:"-"` //DON'T enable it for json (leave it as-is!)!
	Name           string             `bson:",omitempty " json:"name,omitempty" validate:"required"`
	Status         string             `bson:",omitempty" json:"status,omitempty"`
	Type           string             `bson:",omitempty" json:"type,omitempty"`
	Wallet         float64            `bson:",omitempty" json:"wallet"`
	LastLogin      time.Time          `bson:",omitempty" json:"lastLogin,omitempty"`
	CreatedByAdmin bool               `bson:"," json:"createdByAdmin"`
	Audit          *utils.Audit       `bson:",omitempty" json:"audit,omitempty"`
}

// Token Verification

type VerificationToken struct {
	Id         primitive.ObjectID `bson:"_id,omitempty" json:"-"`
	UUID       string             `bson:",omitempty" json:"uuid, omitempty"`
	TokenType  string             `bson:",omitempty" json:",omitempty"`
	Token      string             `bson:",omitempty" json:"token, omitempty"`
	ExpiryTime time.Time          `bson:",omitempty" json:"expiryTime, omitempty"`
	Status     string             `bson:",omitempty" json:"status, omitempty"`
}

func (v VerificationToken) Clone(tokenType VerificationTokenType) *VerificationToken {
	v.TokenType = tokenType.String()
	return &v
}

func (v VerificationToken) IsExpired() bool {
	return time.Now().After(v.ExpiryTime)
}

func (v *VerificationToken) SetExpiryTime(duration time.Duration) {
	v.ExpiryTime = time.Now().Add(duration)
}

func DefaultExpiryTime() time.Time {
	return time.Now().Add(constants.TokenExpiryDuration * time.Hour)
}

// Constants And Enums

const VerificationTokenStatusActive string = "Active"
const VerificationTokenStatusUsed string = "Used"

type VerificationTokenType int

const (
	ACCOUNTACTIVATION VerificationTokenType = iota
	PASSWORDRESET
)

func (e VerificationTokenType) String() string {
	return [...]string{"AccountActivation", "PasswordReset"}[e]
}

type UserStatus int

const (
	USER_STATUS_PENDING_ACTIVATION UserStatus = iota
	USER_STATUS_ACTIVE
	USER_STATUS_DEMISED
)

func (s UserStatus) String() string {
	return [...]string{"PendingActivation", "Active", "Demised"}[s]
}

type UserType int

const (
	USER_TYPE_INVESTOR UserType = iota
	USER_TYPE_BO
)

func (s UserType) String() string {
	return [...]string{"Investor", "Business Owner"}[s]
}

type UserLoginResponse struct {
	User          *User          `json:"user"`
	JwtToken      string         `json:"jwtToken"`
	BusinessOwner *BusinessOwner `json:"businessOwner"`
}

type SignUpRequest struct {
	Email          string `json:"email" validate:"required,email"`
	Password       string `json:"password" validate:"required"`
	Type           string `json:"type" validate:"required"`
	CreatedByAdmin bool   `json:"createdByAdmin,omitempty"`
}

type SignInRequest struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
	UserType string `json:"userType"`
}

type TokenBasedPasswordResetRequest struct {
	Token    string `json:"token" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type EmailAndUserType struct {
	Email    string `json:"email" validate:"required,email"`
	UserType string `json:"userType" validate:"required"`
}

type UpdatePasswordRequest struct {
	UUID        string `json:"uuid" validate:"required"`
	OldPassword string `json:"oldPassword" validate:"required"`
	NewPassword string `json:"newPassword" validate:"required"`
}

type ActivateAccountRequest struct {
	Email    string `json:"email" validate:"required,email"`
	UserType string `json:"userType" validate:"required"`
	Token    string `json:"token" validate:"required"`
}

func GetUserServiceTypes() []string {
	return []string{"hipl", "arcor", "helicap"}
}
