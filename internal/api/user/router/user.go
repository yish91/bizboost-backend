package router

import (
	"bizboost/internal/api/user/db"
	"bizboost/internal/api/user/handler"
	"bizboost/internal/api/user/service"
	"bizboost/internal/platform/authentication"

	platformDb "bizboost/internal/platform/db"

	"github.com/go-chi/chi"
)

func SetUserRoutes(router *chi.Mux) *chi.Mux {

	h := handler.NewUserHandlerImpl(service.NewUserSvcImpl(db.NewUserDaoImpl(platformDb.DatabaseStore.GetDatabase())))

	router.Group(func(r chi.Router) {
		r.Post("/user/login", h.UserSignIn)
		r.Post("/user/signUp", h.UserSignUp)
		r.Post("/user/resetPasswordToken", h.GetPasswordResetToken)
		r.Post("/user/resetPassword", h.ResetPassword)
		r.Post("/user/activateAccountToken", h.GetActivateAccountToken)
		r.Post("/user/activateAccount", h.ActivateAccount)
		r.Post("/user/test", h.UserServiceTest)
	})

	router.Group(func(r chi.Router) {
		// validating JWT token
		r.Use(authentication.RequireTokenAuthentication)

		r.Post("/user/updatePassword", h.UpdatePassword)
		r.Post("/user/bo", h.UpsertBusinessOwner)
		r.Get("/user/bo/{id}", h.GetBusinessOwner)
		r.Post("/user/bo/{id}/documents", h.UploadBusinessOwnerDocument)
	})

	return router
}
