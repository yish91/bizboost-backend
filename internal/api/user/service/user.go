package service

import (
	"bizboost/internal/api/user/db"
	"bizboost/internal/api/user/model"
	"bizboost/internal/platform/authentication"
	"bizboost/internal/platform/aws"
	"bizboost/internal/platform/constants"
	"bizboost/internal/platform/utils"
	"mime/multipart"
	"strings"

	userAuthentication "bizboost/internal/api/user/authentication"

	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/mongo"
)

type UserSvc interface {
	UserSignUp(reqModel *model.SignUpRequest) (*model.User, error)
	UserSignIn(reqModel *model.SignInRequest) (*model.UserLoginResponse, error)
	GetPasswordResetToken(reqModel *model.EmailAndUserType) (string, error)
	ResetPassword(reqModel *model.TokenBasedPasswordResetRequest) (*model.User, error)
	GetActivateAccountToken(reqModel *model.EmailAndUserType) (string, error)
	ActivateAccount(reqModel *model.ActivateAccountRequest) (*model.User, error)
	// requires token
	UpdatePassword(reqModel *model.UpdatePasswordRequest) (*model.User, error)

	UserServiceTest(r *http.Request) (string, error)

	UpsertBusinessOwner(reqModel *model.BusinessOwner, userId string) error
	GetBusinessOwner(id string) (*model.BusinessOwner, error)
	UploadBusinessOwnerDocument(ctx context.Context, file multipart.File, fileHeader *multipart.FileHeader, boUUID string, userId string) (string, error)
}

type UserSvcImpl struct {
	userDao db.UserDao
}

func NewUserSvcImpl(userDao db.UserDao) UserSvc {
	return UserSvcImpl{
		userDao: userDao,
	}
}

func (u UserSvcImpl) UserSignUp(reqModel *model.SignUpRequest) (*model.User, error) {
	var result *model.User
	txnErr := utils.RunInTxn(func(sessionContext mongo.SessionContext) error {
		user, err := u.userSignUp(sessionContext, reqModel)
		if err != nil {
			return err
		}
		result = user
		return nil
	})
	if txnErr != nil {
		return nil, txnErr
	}
	return result, nil
}

func (u UserSvcImpl) userSignUp(ctx context.Context, reqModel *model.SignUpRequest) (*model.User, error) {
	existingUser, _ := u.userDao.GetUserByUserTypeAndEmail(ctx, reqModel.Type, reqModel.Email)
	if existingUser != nil {
		errMessage := "Email already exists as - " + reqModel.Type
		return nil, utils.NewServiceError(http.StatusBadRequest, errMessage)
	}

	// Create User
	user := u.createUserModel(reqModel)
	user.Audit = utils.NewAudit(user.UUID, constants.EVENT_USER_SIGNUP)
	if err := u.userDao.CreateUser(ctx, user); err != nil {
		return nil, err
	}

	// saving token for verification
	if err := u.createAndPersistToken(ctx, model.ACCOUNTACTIVATION, user); err != nil {
		return nil, err
	}

	// getting back user
	fetchedUser, err := u.userDao.GetUserByUserTypeAndEmail(ctx, reqModel.Type, reqModel.Email)
	if err != nil {
		errMessage := "Error getting back created User!"
		fmt.Println(errMessage + " | Error : " + err.Error())
		return nil, utils.NewServiceError(http.StatusInternalServerError, errMessage)
	}
	if fetchedUser == nil {
		fmt.Println("User not found after creation, for userType - " + reqModel.Type + "and email - " + reqModel.Email)
		return nil, utils.NewServiceError(http.StatusInternalServerError, "Error creating User!")
	}
	return fetchedUser, nil
}

func (u UserSvcImpl) UserSignIn(reqModel *model.SignInRequest) (*model.UserLoginResponse, error) {
	ctx := context.TODO()
	dbUser, err := u.userDao.GetUserByEmail(ctx, reqModel.Email)
	if err != nil {
		errMessage := "Error getting User!"
		fmt.Println(errMessage + " | Error : " + err.Error())
		return nil, utils.NewServiceError(http.StatusBadRequest, errMessage)
	}
	if dbUser == nil {
		fmt.Println("User Not Found!")
		return nil, utils.NewServiceError(http.StatusBadRequest, "Invalid Email!")
	}

	// check if user is active
	if dbUser.Status != model.USER_STATUS_ACTIVE.String() {
		return nil, utils.NewServiceError(http.StatusUnauthorized, "User status is not active!")
	}

	// compare passwords
	passwordMatched := authentication.ComparePasswords(dbUser.HashedPassword, reqModel.Password)
	if !passwordMatched {
		return nil, utils.NewServiceError(http.StatusUnauthorized, "Incorrect Password!")
	}

	token, err := authentication.InitJWTAuthenticationBackend().GenerateTokenWithJWTExpDelta(dbUser.UUID)
	if err != nil {
		// If there is an error in creating the JWT return an internal server error
		return nil, err
	}
	previousLastLogin := dbUser.LastLogin
	newLastLogin := time.Now()
	dbUser.LastLogin = newLastLogin
	err = u.userDao.UpdateUser(ctx, dbUser)
	if err != nil {
		return nil, err
	}
	//Intentionally, sending the original version of user (instead of fetching back the updated version)
	//as we should send back lastLogin time, not the current login time
	dbUser.LastLogin = previousLastLogin
	ulr := &model.UserLoginResponse{
		User:     dbUser,
		JwtToken: token,
	}
	if dbUser.Type == model.USER_TYPE_BO.String() {
		bo, err := u.userDao.GetBusinessOwner(ctx, dbUser.UUID)
		if err == nil {
			ulr.BusinessOwner = bo
		}
	}
	return ulr, nil
}

// creating token and getting password reset token
func (u UserSvcImpl) GetPasswordResetToken(reqModel *model.EmailAndUserType) (string, error) {
	return u.getToken(reqModel, model.PASSWORDRESET)
}

// reseting password using reset password token
func (u UserSvcImpl) ResetPassword(reqModel *model.TokenBasedPasswordResetRequest) (*model.User, error) {
	var updatedUser *model.User
	txnError := utils.RunInTxn(func(ctx mongo.SessionContext) error {
		dbToken, _ := u.userDao.GetActiveVerificationToken(ctx, model.PASSWORDRESET.String(), reqModel.Token)
		if dbToken == nil {
			return utils.NewServiceError(http.StatusBadRequest, "Invalid Token!")
		}
		if dbToken.IsExpired() {
			return utils.NewServiceError(http.StatusBadRequest, "Expired Token!")
		}

		user, err := u.userDao.GetUserByUUID(ctx, dbToken.UUID)
		if err != nil {
			errMessage := "User Not Found!"
			fmt.Println(errMessage + " | Error - " + err.Error())
			return utils.NewServiceError(http.StatusInternalServerError, errMessage)
		}

		// update password
		audit := utils.NewAudit(user.UUID, constants.EVENT_USER_RESET_PASSWORD)
		if err := u.updateUserPassword(ctx, audit, user, reqModel.Password); err != nil {
			fmt.Println("Failed to update user password - " + err.Error())
			return utils.NewServiceError(http.StatusInternalServerError, "Failed to update password")
		}
		// update token status
		if err := u.updateVerificationTokenStatus(ctx, dbToken); err != nil {
			fmt.Println("Failed to update token status - " + err.Error())
			return utils.NewServiceError(http.StatusInternalServerError, "Failed to update password")
		}

		// getting back updated user
		userFromDb, err := u.userDao.GetUserByUUID(ctx, dbToken.UUID)
		if err != nil {
			fmt.Println("Error getting back updated user - " + " | Error : " + err.Error())
			return utils.NewServiceError(http.StatusInternalServerError, "Failed to update password")
		}
		updatedUser = userFromDb
		return nil
	})

	if txnError != nil {
		return nil, txnError
	}
	return updatedUser, nil
}

// creating activation token
func (u UserSvcImpl) GetActivateAccountToken(reqModel *model.EmailAndUserType) (string, error) {
	return u.getToken(reqModel, model.ACCOUNTACTIVATION)
}

// activating account using token
func (u UserSvcImpl) ActivateAccount(reqModel *model.ActivateAccountRequest) (*model.User, error) {
	var updatedUser *model.User
	txnError := utils.RunInTxn(func(ctx mongo.SessionContext) error {
		dbToken, _ := u.userDao.GetVerificationToken(ctx, model.ACCOUNTACTIVATION.String(), reqModel.Token)
		if dbToken == nil {
			return utils.NewServiceError(http.StatusBadRequest, "Invalid Token!")
		}
		if dbToken.IsExpired() {
			return utils.NewServiceError(http.StatusBadRequest, "Expired Token!")
		}

		user, err := u.userDao.GetUserByUserTypeAndEmail(ctx, reqModel.UserType, reqModel.Email)
		if err != nil {
			errMessage := "User Not Found!"
			fmt.Println(errMessage + " | Error - " + err.Error())
			return utils.NewServiceError(http.StatusInternalServerError, errMessage)
		}

		// proceed account activation, only if user status is pending activation
		if user.Status != model.USER_STATUS_PENDING_ACTIVATION.String() {
			return utils.NewServiceError(http.StatusBadRequest, "User is already "+user.Status)
		}

		// updating user
		user.Status = model.USER_STATUS_ACTIVE.String()
		user.Audit = utils.NewAudit(user.UUID, constants.EVENT_USER_STATUS_ACTIVE)

		if err := u.userDao.UpdateUser(ctx, user); err != nil {
			fmt.Println("Error updating user status - " + " | Error : " + err.Error())
			return utils.NewServiceError(http.StatusInternalServerError, "Error updating user status!")
		}
		// updating token status to uesd
		if err := u.updateVerificationTokenStatus(ctx, dbToken); err != nil {
			fmt.Println("Failed to update token status : UUID - " + user.UUID + " | Error - " + err.Error())
			return utils.NewServiceError(http.StatusInternalServerError, "Failed to Activate User")
		}

		// getting back updated user
		userFromDb, err := u.userDao.GetUserByUserTypeAndEmail(ctx, reqModel.UserType, reqModel.Email)
		if err != nil {
			fmt.Println("Error getting back updated user - " + " | Error : " + err.Error())
			return utils.NewServiceError(http.StatusInternalServerError, "Error getting back updated user!")
		}
		updatedUser = userFromDb
		return nil
	})

	if txnError != nil {
		return nil, txnError
	}
	return updatedUser, nil
}

func (u UserSvcImpl) UpdatePassword(reqModel *model.UpdatePasswordRequest) (*model.User, error) {
	var updatedUser *model.User
	txnError := utils.RunInTxn(func(ctx mongo.SessionContext) error {
		user, err := u.userDao.GetUserByUUID(ctx, reqModel.UUID)
		if err != nil {
			fmt.Println("Error getting user - " + " | Error : " + err.Error())
			return utils.NewServiceError(http.StatusBadRequest, "User not found!")
		}
		if user == nil {
			return utils.NewServiceError(http.StatusBadRequest, "User not found!")
		}

		// validate old password
		if !authentication.ComparePasswords(user.HashedPassword, reqModel.OldPassword) {
			return utils.NewServiceError(http.StatusBadRequest, "Password do not match!")
		}

		// update password
		audit := utils.NewAudit(reqModel.UUID, constants.EVENT_USER_UPDATE_PASSWORD)
		if err := u.updateUserPassword(ctx, audit, user, reqModel.NewPassword); err != nil {
			fmt.Println("Failed to update user password - | Error :" + err.Error())
			return utils.NewServiceError(http.StatusInternalServerError, "Failed to update password")
		}

		// getting back updated user
		userFromDb, err := u.userDao.GetUserByUUID(ctx, reqModel.UUID)
		if err != nil {
			errMessage := "Error getting back updated user!"
			fmt.Println(errMessage + " | Error : " + err.Error())
			return utils.NewServiceError(http.StatusInternalServerError, errMessage)
		}
		updatedUser = userFromDb
		return nil

	})
	if txnError != nil {
		return nil, txnError
	}
	return updatedUser, nil
}

// Private Functions

// create and persist token
func (u UserSvcImpl) createAndPersistToken(ctx context.Context, tokenType model.VerificationTokenType,
	user *model.User) error {
	var err error
	token := u.createToken(tokenType, user.Email, user.UUID)
	err = u.persistTokens(ctx, token)
	if err != nil {
		errMessage := "Error Persisting Token!"
		fmt.Println(errMessage + " | Error : " + err.Error())
		return utils.NewServiceError(http.StatusInternalServerError, errMessage)
	}
	return nil
}

// creating and sending back token
func (u UserSvcImpl) getToken(reqModel *model.EmailAndUserType, tokenType model.VerificationTokenType) (string, error) {
	var tokenString string
	txnError := utils.RunInTxn(func(ctx mongo.SessionContext) error {
		user, err := u.userDao.GetUserByUserTypeAndEmail(ctx, reqModel.UserType, reqModel.Email)
		if err != nil {
			fmt.Println("Error getting user - " + " | Error : " + err.Error())
			return utils.NewServiceError(http.StatusInternalServerError, "Error sending Email!")
		}
		if user == nil {
			return utils.NewServiceError(http.StatusBadRequest, "Invalid email!")
		}

		// proceed account activation, only if user status is pending activation
		if tokenType == model.ACCOUNTACTIVATION && user.Status != model.USER_STATUS_PENDING_ACTIVATION.String() {
			return utils.NewServiceError(http.StatusBadRequest, "User is already active or demised!")
		}

		// create and persist token
		token := u.createToken(tokenType, user.Email, user.UUID)
		if err := u.persistTokens(ctx, token); err != nil {
			errMessage := "Error sending Email!"
			fmt.Println(errMessage + " | Error : " + err.Error())
			return utils.NewServiceError(http.StatusInternalServerError, errMessage)
		}

		tokenString = token.Token
		return nil
	})

	if txnError != nil {
		return "", txnError
	}
	return tokenString, nil
}

func (u UserSvcImpl) createToken(tokenType model.VerificationTokenType, emailAddress, userID string) *model.VerificationToken {
	return &model.VerificationToken{
		UUID:       userID,
		TokenType:  tokenType.String(),
		Token:      authentication.GenerateVerificationToken(utils.FormatEmail(emailAddress)),
		ExpiryTime: model.DefaultExpiryTime(), //TODO: Need to find a right place to set this!
		Status:     model.VerificationTokenStatusActive,
	}
}

func (u UserSvcImpl) persistTokens(ctx context.Context, tokens ...*model.VerificationToken) error {
	for _, token := range tokens {
		if err := u.userDao.PersistVerificationToken(ctx, token); err != nil {
			return err
		}
	}
	return nil
}

func (u UserSvcImpl) createUserModel(reqModel *model.SignUpRequest) *model.User {
	userID := uuid.New().String()
	return &model.User{
		UUID:           userID,
		Type:           reqModel.Type,
		Email:          reqModel.Email,
		HashedPassword: authentication.HashAndSalt(reqModel.Password),
		Status:         model.USER_STATUS_PENDING_ACTIVATION.String(),
		CreatedByAdmin: reqModel.CreatedByAdmin,
	}
}

func (u UserSvcImpl) updateUserPassword(ctx context.Context, audit *utils.Audit, user *model.User, password string) error {
	user.HashedPassword = authentication.HashAndSalt(password)
	user.Audit = audit
	return u.userDao.UpdateUser(ctx, user)
}

func (u UserSvcImpl) updateVerificationTokenStatus(ctx context.Context, token *model.VerificationToken) error {
	tokenForUpdate := &model.VerificationToken{
		Id:     token.Id,
		Status: model.VerificationTokenStatusUsed,
	}
	return u.userDao.UpdateVerificationToken(ctx, tokenForUpdate)
}

func (u UserSvcImpl) UserServiceTest(r *http.Request) (string, error) {
	return userAuthentication.GetServiceCaller(r)
}

func (u UserSvcImpl) UpsertBusinessOwner(reqModel *model.BusinessOwner, userId string) error {
	ctx := context.TODO()
	reqModel.Audit = utils.NewAuditWithoutEvent(userId)
	return u.userDao.UpsertBusinessOwner(ctx, reqModel)
}

func (u UserSvcImpl) GetBusinessOwner(id string) (*model.BusinessOwner, error) {
	ctx := context.TODO()
	return u.userDao.GetBusinessOwner(ctx, id)
}

func (u UserSvcImpl) UploadBusinessOwnerDocument(ctx context.Context, file multipart.File, fileHeader *multipart.FileHeader, boUUID string, userId string) (string, error) {
	bo, err := u.userDao.GetBusinessOwner(ctx, boUUID)
	if err != nil {
		return "", err
	}
	fileType := strings.Split(fileHeader.Filename, ".")[1]
	size := fileHeader.Size
	buffer := make([]byte, size)
	if _, err := file.Read(buffer); err != nil {
		return "", err
	}

	folder, err := aws.UploadFileToS3(buffer, fileType, boUUID)
	if err != nil {
		return "", err
	}
	bo.Audit = utils.NewAuditWithoutEvent(userId)
	bo.Documents = folder
	err = u.userDao.UpsertBusinessOwner(ctx, bo)
	if err != nil {
		return "", err
	}
	return folder, nil
}
