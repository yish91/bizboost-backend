package authentication

import (
	"bizboost/cmd/settings"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"golang.org/x/crypto/bcrypt"
)

var JWTExpirationDelta int

type JWTAuthenticationBackend interface {
	GetPublicKey() *rsa.PublicKey
	GenerateToken(userUUID string) (string, error)
	GenerateTokenWithJWTExpDelta(userUUID string) (string, error)
	Authenticate(hashedPwd string, plainPwd string) bool
}

type JWTAuthenticationBackendImpl struct {
	privateKey *rsa.PrivateKey
	PublicKey  *rsa.PublicKey
}

const (
	expireOffset = 3600
)

var authBackendInstance JWTAuthenticationBackend

func InitJWTAuthenticationBackend() JWTAuthenticationBackend {
	fmt.Println("authBackendInstance ", authBackendInstance)
	if authBackendInstance == nil {
		authBackendInstance = &JWTAuthenticationBackendImpl{
			privateKey: generatePrivateKey(settings.Get().Secrets.PrivateKey),
			PublicKey:  generatePublicKey(settings.Get().Secrets.PublicKey),
		}
	}

	return authBackendInstance
}

func InitJWTAuthBackendWithInstance(authInstance JWTAuthenticationBackend) {
	authBackendInstance = authInstance
}

func (j *JWTAuthenticationBackendImpl) GetPublicKey() *rsa.PublicKey {
	if authBackendInstance != nil {
		return j.PublicKey
	}
	return nil
}

func (j *JWTAuthenticationBackendImpl) GenerateToken(userUUID string) (string, error) {
	token := jwt.New(jwt.SigningMethodRS512)
	token.Claims = jwt.MapClaims{
		"exp": time.Now().Add(time.Hour * time.Duration(JWTExpirationDelta)).Unix(),
		"iat": time.Now().Unix(),
		"sub": userUUID,
	}
	tokenString, err := token.SignedString(j.privateKey)
	if err != nil {
		panic(err)
	}
	return tokenString, nil
}

func (j *JWTAuthenticationBackendImpl) GenerateTokenWithJWTExpDelta(userUUID string) (string, error) {
	InitJWTAuthenticationBackend()
	JWTExpirationDelta = settings.Get().GeneralConfigs.JWTExpirationDelta
	return j.GenerateToken(userUUID)
}

func (j *JWTAuthenticationBackendImpl) Authenticate(hashedPwd string, plainPwd string) bool {
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, []byte(plainPwd))
	if err != nil {
		log.Println(err)
		return false
	}

	return true
}

func (j *JWTAuthenticationBackendImpl) getTokenRemainingValidity(timestamp interface{}) int {
	if validity, ok := timestamp.(float64); ok {
		tm := time.Unix(int64(validity), 0)
		remainer := tm.Sub(time.Now())
		if remainer > 0 {
			return int(remainer.Seconds() + expireOffset)
		}
	}
	return expireOffset
}

// func (backend *JWTAuthenticationBackend) Logout(tokenString string, token *jwt.Token, port string, platform string) error {
// 	redisConn := redis.Connect(port, platform)
// 	return redisConn.SetValue(tokenString, tokenString, backend.getTokenRemainingValidity(token.Claims.(jwt.MapClaims)["exp"]))
// }

// func (backend *JWTAuthenticationBackend) IsInBlacklist(token string, port string, platform string) bool {
// 	redisConn := redis.Connect(port, platform)
// 	redisToken, _ := redisConn.GetValue(token)

// 	if redisToken == nil {
// 		return false
// 	}

// 	return true
// }

func generatePrivateKey(privateKey string) *rsa.PrivateKey {
	privateKeyStr := "-----BEGIN PRIVATE KEY-----\n" + privateKey + "\n-----END PRIVATE KEY-----"

	r := strings.NewReader(privateKeyStr)
	pembytes, err := ioutil.ReadAll(r)
	if err != nil {
		log.Fatal(err)
	}

	data, _ := pem.Decode([]byte(pembytes))
	privateKeyImported, err := x509.ParsePKCS1PrivateKey(data.Bytes)

	if err != nil {
		panic(err)
	}

	return privateKeyImported
}

func generatePublicKey(publicKey string) *rsa.PublicKey {
	publicKeyStr := "-----BEGIN PUBLIC KEY-----\n" + publicKey + "\n-----END PUBLIC KEY-----"

	r := strings.NewReader(publicKeyStr)
	pembytes, err := ioutil.ReadAll(r)
	if err != nil {
		log.Fatal(err)
	}

	data, _ := pem.Decode([]byte(pembytes))

	rsaPub, err := x509.ParsePKCS1PublicKey(data.Bytes)

	if err != nil {
		panic(err)
	}

	return rsaPub
}

func GetToken(req *http.Request) (*jwt.Token, error) {
	authBackend := InitJWTAuthenticationBackend()
	token, err := request.ParseFromRequest(req, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		} else {
			return authBackend.GetPublicKey(), nil
		}
	})
	return token, err
}

func GetUserId(req *http.Request) (string, error) {
	token, err := GetToken(req)
	if err != nil {
		return "", err
	}
	userId := token.Claims.(jwt.MapClaims)["sub"].(string)
	return userId, nil
}
