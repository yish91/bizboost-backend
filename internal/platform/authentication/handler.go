package authentication

import (
	"net/http"
)

func RequireTokenAuthentication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, err := GetToken(r)
		//fmt.Println(token.Claims.(jwt.MapClaims)["sub"])

		if err == nil && token.Valid {
			next.ServeHTTP(w, r)
		} else {
			w.WriteHeader(http.StatusUnauthorized)
		}
	})
}
