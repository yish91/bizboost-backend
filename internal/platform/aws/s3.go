package aws

import (
	"bizboost/cmd/settings"
	"bizboost/internal/platform/utils"
	"bytes"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awsutil"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"io/ioutil"
	"net/http"
	"net/url"
)

type FileACL int

const (
	// accessible only through our api url (as it required authentication) -- to be used for all files, except possibly images
	FileACLPrivate FileACL = iota
	// can be accessible through direct s3 url (ideally, shouldn't be used except for images possibly)
	FileACLPublicRead
)

func (a FileACL) String() string {
	return [...]string{
		"private", "public-read"}[a]
}

func UploadFileBytes(buffer []byte, folder string, title string, acl FileACL) (fileURL string, error error) {
	awsAccessKeyId := settings.Get().Secrets.AWSAccess
	awsSecretAccessKey := settings.Get().Secrets.AWSSecret
	awsBucket := awsBucket()
	s3Region := s3Region()
	token := ""
	creds := credentials.NewStaticCredentials(awsAccessKeyId, awsSecretAccessKey, token)
	_, err := creds.Get()
	if err != nil {
		return "", err
	}
	cfg := aws.NewConfig().WithRegion("ap-southeast-1").WithCredentials(creds)
	s, err := session.NewSession()
	if err != nil {
		_ = utils.LogError(err)
		return "", err
	}
	svc := s3.New(s, cfg)

	awsObjectKey := folder + title

	resourceURLQuery := folder + url.QueryEscape(title)

	resp, err := svc.PutObject(&s3.PutObjectInput{
		Bucket:               aws.String(awsBucket),
		Key:                  aws.String(awsObjectKey),
		ACL:                  aws.String(acl.String()),
		Body:                 bytes.NewReader(buffer),
		ContentLength:        aws.Int64(int64(len(buffer))),
		ContentType:          aws.String(http.DetectContentType(buffer)),
		ContentDisposition:   aws.String("attachment"),
		ServerSideEncryption: aws.String("AES256"),
		StorageClass:         aws.String("STANDARD"),
	})
	if err != nil {
		return "", err
	}
	fmt.Printf("response %s", awsutil.StringValue(resp))
	resourceURL := "https://" + awsBucket + "." + s3Region + resourceURLQuery
	return resourceURL, err
}

func UploadFileToS3(buffer []byte, fileType string, loanUUID string) (fileURL string, error error) {
	loanbookFolder := BOFolder(loanUUID)
	title := "documents"
	awsAccessKeyId := settings.Get().Secrets.AWSAccess
	awsSecretAccessKey := settings.Get().Secrets.AWSSecret
	awsBucket := awsBucket()
	s3Region := s3Region()
	token := ""
	creds := credentials.NewStaticCredentials(awsAccessKeyId, awsSecretAccessKey, token)
	_, err := creds.Get()
	if err != nil {
		return "", utils.LogError(err)
	}
	cfg := aws.NewConfig().WithRegion("ap-southeast-1").WithCredentials(creds)
	s, err := session.NewSession()
	if err != nil {
		_ = utils.LogError(err)
		return "", err
	}
	svc := s3.New(s, cfg)

	//This basically is the placeholder (path) where file gets uploaded
	awsObjectKey := loanbookFolder + title + "." + fileType

	//Not escaping Folder part for now, as folder names that we are using are good for now
	resourceURLQuery := loanbookFolder + url.QueryEscape(title)

	// config settings: this is where you choose the bucket,
	// filename, content-type and storage class of the file
	// you're uploading
	resp, err := svc.PutObject(&s3.PutObjectInput{
		Bucket:               aws.String(awsBucket),
		Key:                  aws.String(awsObjectKey),
		ACL:                  aws.String("public-read"), // could be private if you want it to be access by only authorized users
		Body:                 bytes.NewReader(buffer),
		ContentLength:        aws.Int64(int64(len(buffer))),
		ContentType:          aws.String(generateContentType(buffer, fileType)),
		ContentDisposition:   aws.String("attachment"),
		ServerSideEncryption: aws.String("AES256"),
		StorageClass:         aws.String("STANDARD"),
	})
	if err != nil {
		return "", err
	}
	fmt.Printf("response %s", awsutil.StringValue(resp))
	resourceURL := "https://" + awsBucket + "." + s3Region + resourceURLQuery + "." + fileType
	return resourceURL, err
}

func GetFileFromS3(key string) ([]byte, error) {
	awsAccessKeyId := settings.Get().Secrets.AWSAccess
	awsSecretAccessKey := settings.Get().Secrets.AWSSecret
	awsBucket := awsBucket()
	token := ""
	creds := credentials.NewStaticCredentials(awsAccessKeyId, awsSecretAccessKey, token)
	_, err := creds.Get()
	if err != nil {
		return nil, err
	}
	cfg := aws.NewConfig().WithRegion("ap-southeast-1").WithCredentials(creds)
	s, err := session.NewSession()
	if err != nil {
		_ = utils.LogError(err)
		return nil, err
	}
	svc := s3.New(s, cfg)
	resp, err := svc.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(awsBucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

func ListObjectsFromS3(prefix string) ([]string, error) {
	var result []string
	awsAccessKeyId := settings.Get().Secrets.AWSAccess
	awsSecretAccessKey := settings.Get().Secrets.AWSSecret
	awsBucket := awsBucket()
	token := ""
	creds := credentials.NewStaticCredentials(awsAccessKeyId, awsSecretAccessKey, token)
	_, err := creds.Get()
	if err != nil {
		return nil, err
	}
	cfg := aws.NewConfig().WithRegion("ap-southeast-1").WithCredentials(creds)
	s, err := session.NewSession()
	if err != nil {
		_ = utils.LogError(err)
		return nil, err
	}
	svc := s3.New(s, cfg)
	resp, err := svc.ListObjects(&s3.ListObjectsInput{
		Bucket: aws.String(awsBucket),
		Prefix: aws.String(prefix),
	})
	if err != nil {
		return result, utils.LogError(err)
	}
	for _, object := range resp.Contents {
		key := *object.Key
		result = append(result, key)
	}
	return result, nil
}

func awsBucket() string {
	return "bizboost"
}

func s3Region() string {
	return "s3-ap-southeast-1.amazonaws.com/"
}

func BOFolder(boUUID string) string {
	return "businessowner/" + boUUID + "/"
}

//func getS3PathFromUrl(url string) string {
//	s3Region := s3Region()
//	strArr := strings.Split(url, s3Region)
//	return strArr[len(strArr)-1]
//}

func generateContentType(buffer []byte, fileType string) string {
	switch fileType {
	case "zip":
		return "application/zip"
	case "xlsx":
		return "binary/octet-stream"
	case "csv":
		return "text/csv; charset=utf-8"
	default:
		return http.DetectContentType(buffer)
	}
}
