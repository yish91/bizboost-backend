package constants

import "time"

//TODO: These need to be updated!
const CompanyName string = "Helicap Investments"
const CompanyAuthorizedRepresentative string = "Quentin Dominique C Vanoekel"
const ApiPrefix string = "/api"
const MultiCurrency string = "Multi-CCY"
const AdminEmail_Prod string = "investor@heli-cap.com"
const AdminEmail_QA string = "hiplqa@heli-cap.com"
const TokenExpiryDuration time.Duration = 720 // i.e. 720 hours -> 30 days

//Database Collections List
const (
	Collection_User          = "user"
	Collection_Token         = "token"
	Collection_Loan          = "loan"
	Collection_Bid           = "bid"
	Collection_Repayment     = "repayment"
	Collection_Withdrawal    = "withdrawal"
	Collection_BusinessOwner = "businessowner"
	Collection_Deposit       = "deposit"
)
