package constants

type Event int

//TODO: Confirm if the unused Events will have any use-case in future and clean-up accordingly
//NOTE: The entries in this const and it's corresponding String() has to be in exact sequence
//Hence, please ensure if you are adding/editing entries here, corresponding string is changed accordingly
//Or else, we will have mismatched entries!
const (
	//--User Related Events--
	EVENT_USER_SIGNUP = iota
	EVENT_USER_LOGIN
	EVENT_USER_RESET_PASSWORD
	EVENT_USER_STATUS_ACTIVE
	EVENT_USER_UPDATE_PASSWORD
)

func (e Event) String() string {
	return [...]string{
		//User Related Events
		"UserSignUp",
		"UserLogin",
		"UserResetPassword",
		"UserActivate",
		"UserUpdatePassword",
	}[e]
}
