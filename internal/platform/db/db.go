package db

import (
	"context"
	"errors"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func FindAll(ctx context.Context, db *mongo.Database, collectionName string, filter bson.D, result interface{}) error {
	cursor, err := db.Collection(collectionName).Find(ctx, filter)
	if err != nil {
		return err
	}
	return cursor.All(ctx, result)
}

func FindAllAggregate(ctx context.Context, db *mongo.Database, collectionName string, aggregationPipeline []bson.M, result interface{}) error {
	cursor, err := db.Collection(collectionName).Aggregate(ctx, aggregationPipeline)
	if err != nil {
		return err
	}
	return cursor.All(ctx, result)
}

func (store *StoreImpl) RunInTxn(run func(sessionContext mongo.SessionContext) error) error {
	return DatabaseStore.GetMongoClient().UseSession(context.TODO(), func(sessionContext mongo.SessionContext) error {
		if err := sessionContext.StartTransaction(); err != nil {
			fmt.Println("Error during StartTransaction  - " + err.Error())
			return err
		}

		if err := run(sessionContext); err != nil {
			runErrorMessage := "Error during run function - " + err.Error()

			if err := sessionContext.AbortTransaction(sessionContext); err != nil {
				fmt.Println("Error during AbortTransaction (Aborted Because: " + runErrorMessage + ") - " + err.Error())
			} else {
				fmt.Println(runErrorMessage)
			}

			return err
		}

		if err := sessionContext.CommitTransaction(sessionContext); err != nil {
			fmt.Println("Error during CommitTransaction - " + err.Error())
			return errors.New("Some System Issue - please contact our Admin Team")
		}
		return nil
	})

	//TODO: Should we instead panic if error during AbortTransaction & CommitTransaction?
}
