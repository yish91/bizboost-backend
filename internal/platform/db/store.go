package db

import (
	"go.mongodb.org/mongo-driver/mongo"
)

// Our store will have two methods, to add a new bird,
// and to get all existing birds
// Each method returns an error, in case something goes wrong
type Store interface {
	GetMongoClient() *mongo.Client
	GetDatabase() *mongo.Database

	RunInTxn(run func(sessionContext mongo.SessionContext) error) error
}

// The `dbStore` struct will implement the `Store` interface
// It also takes the sql DB connection object, which represents
// the database connection.
type StoreImpl struct {
	MongoClient *mongo.Client
	Db          *mongo.Database
}

func (store *StoreImpl) GetDatabase() *mongo.Database {
	return store.Db
}

func (store *StoreImpl) GetMongoClient() *mongo.Client {
	return store.MongoClient
}

// The store variable is a package level variable that will be available for
// use throughout our application code
var DatabaseStore Store

/*
We will need to call the InitStore method to initialize the store. This will
typically be done at the beginning of our application (in this case, when the server starts up)
This can also be used to set up the store as a mock, which we will be observing
later on
*/
func InitStore(s Store) {
	DatabaseStore = s
}
