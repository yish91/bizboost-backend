package email

import (
	"bizboost/cmd/settings"
	"bizboost/internal/platform/constants"
	"fmt"
	"log"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

var fromEmail *mail.Email = mail.NewEmail(constants.CompanyName, "no-reply@heli-cap.com")

//TODO: Confirm error handling  & logging
func SendEmail(email *EmailTemplate) error {
	//message := mail.NewSingleEmail(email.From, email.Subject, email.To, plainTextContent, htmlContent)
	sendGridMail := newSendGridEmail(email)
	client := sendgrid.NewSendClient(settings.Get().Secrets.SendGridKey)
	response, err := client.Send(sendGridMail)
	if err != nil {
		log.Println(err)
		return err
	} else {
		fmt.Println(response.StatusCode)
		fmt.Println(response.Body)
		fmt.Println(response.Headers)

		return nil
	}
}

func newSendGridEmail(emailTemplate *EmailTemplate) *mail.SGMailV3 {
	m := new(mail.SGMailV3)
	m.SetFrom(fromEmail)
	m.Subject = emailTemplate.Subject
	m.AddContent(mail.NewContent("text/html", emailTemplate.Content))
	if emailTemplate.ReplyTo != nil {
		m.SetReplyTo(emailTemplate.ReplyTo)
	}
	p := mail.NewPersonalization()
	p.AddTos(emailTemplate.Tos...)
	p.AddCCs(emailTemplate.CCs...)
	p.AddBCCs(emailTemplate.BCCs...)
	m.AddPersonalizations(p)

	return m
}

func buildEmailTemplate(subject string, content string, to ...*mail.Email) *EmailTemplate {
	emailTemplate := &EmailTemplate{
		Subject: subject,
		Content: content,
	}
	emailTemplate.AddTos(to...)
	return emailTemplate
}

func email(emailAddress string) *mail.Email {
	return mail.NewEmail("", emailAddress)
}

// TODO: Need to update
func getURLWithToken(urlPrefix string, userType string, token string) string {
	// switch userType {
	// case entity.USER_TYPE_INVESTOR.String():
	// 	return settings.Get().GeneralConfigs.InvestorPortalURL + urlPrefix + token
	// case entity.USER_TYPE_ADMIN.String():
	// 	return settings.Get().GeneralConfigs.AdminPortalURL + urlPrefix + token
	// default:
	// 	return "" //TODO: Decide what to do in such cases!
	// }

	return settings.Get().GeneralConfigs.InvestorPortalURL + urlPrefix + token
}

// TODO: Need to update
func loginURL(userType string) string {
	// switch userType {
	// case entity.USER_TYPE_INVESTOR.String():
	// 	return settings.Get().GeneralConfigs.InvestorPortalURL + "/login"
	// case entity.USER_TYPE_ADMIN.String():
	// 	return settings.Get().GeneralConfigs.AdminPortalURL + "/login"
	// default:
	// 	return "" //TODO: Decide what to do in such cases!
	// }

	return settings.Get().GeneralConfigs.InvestorPortalURL + "/login"
}

func AdminEmail() *mail.Email {
	var result *mail.Email
	if settings.GetEnvironment() == settings.ENV_PROD {
		//TODO: Check and Change name/address
		result = mail.NewEmail("Helicap Investments", constants.AdminEmail_Prod)
	} else {
		result = mail.NewEmail("Helicap Investments", constants.AdminEmail_QA)
	}
	return result
}

//TODO: Confirm and remove, if not required
func fundEmail() *mail.Email {
	var result *mail.Email
	if settings.GetEnvironment() == settings.ENV_PROD {
		//TODO: Check and Change name/address
		result = mail.NewEmail("Helicap Investments", constants.AdminEmail_Prod)
	} else {
		result = mail.NewEmail("Helicap Investments", constants.AdminEmail_QA)
	}
	return result
}

//TODO: tokenize the email, phone numbers and website (do the same for emailBodyContent model creation as well!
//TODO: Need to change the Logo URL!
func formattedEmailContent(contentTemplate *FormattedEmailContentTemplate) string {

	return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Verify your email address</title>
	<style type="text/css" rel="stylesheet" media="all">
		/* Base ------------------------------ */

		*:not(br):not(tr):not(html) {
		font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;
		-webkit-box-sizing: border-box;
		box-sizing: border-box;
	}

	.offset-md-2 {
		margin-left: 4.666667%;
	}

	.logo {
		width: 50px;
		padding: 26px 24px 1px;
	}

	.text-center {
		text-align: center!important;
	}

	.col-md-8 {
		-ms-flex: 0 0 66.666667%;
		background-color: #F5F7F9;
	flex: 0 0 66.666667%;
		max-width: 90.666667%;
	}

	.text-center {
		text-align: center!important;
	}

	.jumbotron {
		padding: 2rem 0rem;
		margin-bottom: 2rem;
		background-color: #fff;
	}

	.card-title {
		margin-bottom: .75rem;
		color: gray;
		font-size: 20px;
		font-weight: normal;
	}

	.title-border {
		width: 100px;
		margin-top: 15px;
		border-color: #9B7C00 !important;
		margin-bottom: 35px;
		border: 1px;
		border-style: solid;
		font-weight: bold !important;
	}

	.d-flex {
		display: -ms-flexbox!important;
		display: flex!important;
	}

	.justify-content-center {
		-ms-flex-pack: center!important;
		justify-content: center!important;
	}

	p.card-text.my-2 {
		font-size: 14px;
		color: rgb(118, 129, 135);
		padding: 2% 5%;
		line-height: 26px;
	}

	.title-border-last {
		width: 12px;
		font-weight: normal !important;
		margin-top: 1rem;
		margin-bottom: 1rem;
		display: none;
		border: 1px;
		border-color: #9B7C00 !important;
		border-style: dashed;
	}

	.text-center {
		text-align: center!important;
	}

	.mb-4,
	.my-4 {
		margin-bottom: 1.5rem!important;
	}

	p.copyright-italic.mb-3 {
		font-size: 13px;
	padding: 0% 8%;
	color: #9EA2A4;
		text-align: center;
	}

	a {
		color: #16365C;
		text-decoration: none;
		background-color: transparent;
	}

	.title-border-bottom {
		width: 100%;
		font-weight: normal !important;
		margin-top: 1rem;
		margin-bottom: 0rem;
		border: 1px;
		border-color: lightgrey !important;
		border-style: solid;
	}

	p.copyright.mb-3 {
		font-size: 9px;
	padding: 2% 6% 4%;
		/* color: #626363;
		   background-color: #25292F; */
		text-align: center;
	}

	.lead {
		font-size: 1.25rem;
		font-weight: 300;
		padding: 20px 0;
	}

	.btn-cyan {
		background-color: #16365C;
		color: #fff;
		border-radius: inherit;
		font-size: 17px;
		margin-top: 11%;
	}

	.btn-lg {
		line-height: 1.5;
		border-radius: inherit;
		padding: 11px 17px 9px;
		font-size: 15px;
		color:#fff !important;
	}

	.saturate {
		filter: saturate(3);
	}

	.grayscale {
		filter: grayscale(100%);
	}

	.contrast {
		filter: contrast(160%);
	}

	.brightness {
		filter: brightness(0.25);
	}

	.blur {
		filter: blur(3px);
	}

	.invert {
		filter: invert(100%);
	}

	.sepia {
		filter: sepia(100%);
	}

	.huerotate {
		filter: hue-rotate(180deg);
	}

	.rss.opacity {
		filter: opacity(50%);
	}
	.graybackground
	{
		background-color:#F5F7F9;
	}
	body {
		width: 100% !important;
		height: 100%;
		margin: 0;
		line-height: 1.4;
		background-color: #F5F7F9;
		color: #839197;
		-webkit-text-size-adjust: none;
	}
	/*Media Queries ------------------------------ */

	@media only screen and (max-width: 600px) {
		.email-body_inner,
		.email-footer {
			width: 100% !important;
		}
	}

	@media only screen and (max-width: 500px) {
		.button {
			width: 100% !important;
		}
	}
	@media only screen and (min-width: 728px) {
		.col-md-8 {
			max-width: 45.666667%;
		}
		.offset-md-2 {
			margin-left: 27.666667%;
		}
		.jumbotron {
			padding: 2rem 0rem;
		}
		.logo {
			width: 210px;
		}
	}

	#repayment {
		border-collapse: collapse;
		margin: auto;
		text-align: left;
		border-collapse: collapse;
		font-size: 13px;
		color: rgb(118,129,135);
		width: 90%;
	}
	#repayment td, #repayment th {
		border: 1px solid #ddd;
		padding: 8px 10px;
	}
	#repayment td{
		min-width: 110px;
	}
	</style>
	</head>

	<body>
	<div class="graybackground">
	<div class="col-md-8 offset-md-2">
	<div class="graybackground">
	<div class="text-center darken-grey-text mb-4">
	<img class="logo brightness" src="https://heli-cap.com/images/helicap_investments.png">
	</div>
	</div>

	<div class="jumbotron text-center">

	<div class="card-title h2-responsive mt-2 font-bold"><strong>` + contentTemplate.EmailTitle + `</strong></div>
	<hr class="title-border">

	<div class="justify-content-center">
	<p class="card-text my-2"> ` + contentTemplate.Text1 + `
	</p>

	</div>` + getButtonText(contentTemplate) +

		`<div class="justify-content-center">
	<p class="card-text my-2" style="padding: 0;margin: 0;">` + contentTemplate.Text2 + `</p>
	</div>

	</div>
	<hr class="title-border-last">
	<div class="graybackground">
	<div class="text-center darken-grey-text mb-4">
	<p class="copyright-italic mb-3">For any queries, you can reach us at
	<br> <a href="mailto:` + AdminEmail().Address + `">` + AdminEmail().Address + `</a>
	<br><a href="https://www.heli-cap.com/hipl" target="_blank">www.heli-cap.com</a></p>
	<hr class="title-border-bottom">
	<p class="copyright mb-3">Helicap Investments Pte Ltd is registered in Singapore, and is a Registered Fund Management Company with the Monetary Authority of Singapore.</p>

	</div>
	</div>

	</div>
	</div>
	</body>

	</html>`
}

func getButtonText(contentTemplate *FormattedEmailContentTemplate) string {
	if contentTemplate.ButtonTitle == "" {
		return ""
	}
	return `<div class="justify-content-center">
		<p class="lead">
		<a class="btn btn-cyan btn-lg"  href="` + contentTemplate.ButtonUrl + `" role="button">` + contentTemplate.ButtonTitle + `</a>
		</p>
	</div>`
}
