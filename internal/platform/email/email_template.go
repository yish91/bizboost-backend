package email

import (
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

type EmailTemplate struct {
	Subject string
	Content string
	Tos     []*mail.Email
	CCs     []*mail.Email
	BCCs    []*mail.Email
	ReplyTo *mail.Email
}

type FormattedEmailContentTemplate struct {
	EmailTitle  string
	Text1       string
	ButtonTitle string
	ButtonUrl   string
	Text2       string
}

func (e *EmailTemplate) WithReplyTo(email *mail.Email) *EmailTemplate {
	e.ReplyTo = email
	return e
}

// AddTos ...
func (e *EmailTemplate) AddTos(to ...*mail.Email) *EmailTemplate {
	e.Tos = append(e.Tos, to...)
	return e
}

// AddCCs ...
func (e *EmailTemplate) AddCCs(cc ...*mail.Email) *EmailTemplate {
	e.CCs = append(e.CCs, cc...)
	return e
}

// AddBCCs ...
func (e *EmailTemplate) AddBCCs(bcc ...*mail.Email) *EmailTemplate {
	e.BCCs = append(e.BCCs, bcc...)
	return e
}
