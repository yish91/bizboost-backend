package utils

import (
	"bizboost/internal/platform/constants"
	"time"
)

type Audit struct {
	UpdatedAt time.Time `bson:",omitempty" json:"updatedAt,omitempty"`
	UpdatedBy string    `bson:",omitempty" json:"updatedBy,omitempty"`
	Event     string    `bson:",omitempty" json:"event,omitempty"`
}

func NewAudit(updatedBy string, event constants.Event) *Audit {
	return &Audit{
		UpdatedAt: time.Now(),
		UpdatedBy: updatedBy,
		Event:     event.String()}
}

func NewAuditWithoutEvent(updatedBy string) *Audit {
	return &Audit{
		UpdatedAt: time.Now(),
		UpdatedBy: updatedBy}
}

