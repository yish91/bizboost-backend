package utils

import (
	"encoding/json"
	"errors"
	"github.com/araddon/dateparse"
	"math"
	"strconv"
	"strings"
	"time"
)

const (
	LayoutISO       = "2006-01-02"
	ExcelLayoutISO1 = "Tue Aug 01 00:00:00 SGT 2019"
	ExcelLayoutISO2 = "01 June 2019"
	ExcelLayoutISO3 = "05/30/2017"
)

func GetLocalisedDate() time.Time {
	loc, _ := time.LoadLocation("Asia/Singapore")
	return time.Now().In(loc)
}

func ParseStringToDate(dateStr string) (time.Time, error) {
	if dateStr == "" {
		return time.Time{}, errors.New("no datestr found")
	}
	return time.Parse(LayoutISO, dateStr)
}

func ParseExcelStringToDate(dateStr string) (time.Time, error) {
	if dateStr == "" {
		return time.Time{}, errors.New("no datestr found")
	}
	result, err := time.Parse(LayoutISO, dateStr)
	if err != nil {
		result, err = time.Parse(ExcelLayoutISO1, dateStr)
		if err != nil {
			result, err = time.Parse(ExcelLayoutISO2, dateStr)
			if err != nil {
				result, err = time.Parse(ExcelLayoutISO3, dateStr)
				if err != nil {
					if strings.Contains(dateStr, "-") {
						newDateStr := strings.ReplaceAll(dateStr, "-", "/")
						result, err = dateparse.ParseAny(newDateStr)
					}
					if err != nil {
						result, err = dateparse.ParseAny(dateStr)
						if err != nil {
							floatSince1900, err := strconv.ParseFloat(dateStr, 64)
							if err != nil {
								return time.Time{}, err
							}
							intSince1900 := int(floatSince1900)
							excelStartTime := time.Date(
								1900, 1, 1, 00, 00, 00, 0, time.UTC)
							return excelStartTime.AddDate(0, 0, intSince1900-2), nil
						}
					}
				}
			}
			return result, err
		}
	}
	return result, err
}

func GetDateFromDict(dict map[string]string, disbursementDateField string, disbursementDayField string, disbursementMonthField string, disbursementYearField string) (time.Time, error) {
	if len(disbursementDateField) != 0 {
		disbursementDateStr := dict[disbursementDateField]
		return ParseExcelStringToDate(disbursementDateStr)
	} else {
		disbursementDay := 1
		disbursementDayStr := dict[disbursementDayField]
		if len(disbursementDayStr) != 0 {
			disbursementDayInt, err := strconv.Atoi(disbursementDayStr)
			disbursementDay = disbursementDayInt
			if err != nil {
				return time.Time{}, err
			}
		}
		disbursementMonthInt, err := strconv.Atoi(dict[disbursementMonthField])
		if err != nil {
			return time.Time{}, err
		}
		disbursementYear, err := strconv.Atoi(dict[disbursementYearField])
		if err != nil {
			return time.Time{}, err
		}
		result := time.Date(
			disbursementYear, time.Month(disbursementMonthInt), disbursementDay, 00, 00, 00, 0, time.UTC)
		return result, nil
	}
}

func DifferenceBetweenMaturityDateAndYearMonth(maturityDate time.Time, month int, year int) int {
	yearMonthPlusOne := time.Date(
		year, time.Month(month+1), 1, 00, 00, 00, 0, time.UTC)
	yearMonth := yearMonthPlusOne.AddDate(0, 0, -1)
	return DifferenceBetweenTwoDates(maturityDate, yearMonth)
}

func DifferenceBetweenTwoDates(a time.Time, b time.Time) int {
	return int(math.Abs(a.Sub(b).Hours() / 24))
}

func CheckIfDateEqualsYearMonth(date time.Time, month int, year int) bool {
	return date.Month() == time.Month(month) && date.Year() == year
}

func GetEndOfMonthFromYearMonth(month int, year int) int {
	yearMonthPlusOne := time.Date(
		year, time.Month(month+1), 1, 00, 00, 00, 0, time.UTC)
	yearMonth := yearMonthPlusOne.AddDate(0, 0, -1)
	return yearMonth.Day()
}

type DateOnly struct {
	Date time.Time `bson:",omitempty" json:"date,omitempty" validate:"required"`
}

type dateOnlyString struct {
	Date string `bson:",omitempty" json:"date,omitempty"`
}

func (d DateOnly) MarshalJSON() ([]byte, error) {
	dateOnlyString := &dateOnlyString{
		Date: dateToString(d.Date),
	}
	return json.Marshal(dateOnlyString)
}

func (d *DateOnly) UnmarshalJSON(data []byte) error {
	var dateOnlyString dateOnlyString
	err := json.Unmarshal(data, &dateOnlyString)
	if err != nil {
		return err
	}
	date, err := StringToDate(dateOnlyString.Date)
	if err != nil {
		return err
	}

	d.Date = date
	return nil
}

func dateToString(d time.Time) string {
	return d.Format(LayoutISO)
}

func StringToDate(d string) (time.Time, error) {
	return time.Parse(LayoutISO, d)
}
