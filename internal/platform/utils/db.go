package utils

import (
	"bizboost/internal/platform/db"
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func FindAll(ctx context.Context, db *mongo.Database, collectionName string, filter bson.D, result interface{}) error {
	cursor, err := db.Collection(collectionName).Find(ctx, filter)
	if err != nil {
		return err
	}
	return cursor.All(ctx, result)
}

func RunInTxn(run func(sessionContext mongo.SessionContext) error) error {
	return db.DatabaseStore.GetMongoClient().UseSession(context.TODO(), func(sessionContext mongo.SessionContext) error {
		if err := sessionContext.StartTransaction(); err != nil {
			return errors.New("Error during StartTransaction  - " + err.Error())
		}

		if err := run(sessionContext); err != nil {
			runErrorMessage := "Error during run function - " + err.Error()

			if err := sessionContext.AbortTransaction(sessionContext); err != nil {
				return errors.New("Error during AbortTransaction (Aborted Because: " + runErrorMessage + ") - " + err.Error())
			}

			return errors.New(runErrorMessage)
		}

		if err := sessionContext.CommitTransaction(sessionContext); err != nil {
			return errors.New("Error during CommitTransaction - " + err.Error())
		}
		return nil
	})

	//TODO: Should we instead panic if error during AbortTransaction & CommitTransaction?
}
