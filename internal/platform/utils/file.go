package utils

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/google/uuid"
	pdfApi "github.com/pdfcpu/pdfcpu/pkg/api"
	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu"
)

const ContentTypePdf = "application/pdf"

func AddWaterMarkToPdf(fileBytes []byte, waterMarkMessage string) ([]byte, error) {
	if !IsPdf(fileBytes) {
		return nil, errors.New("cannot watermark - file is not pdf")
	}
	randomStringForTempFiles := uuid.New().String()
	tempFileName := randomStringForTempFiles + "_temp.pdf"
	if err := ioutil.WriteFile(tempFileName, fileBytes, 0666); err != nil {
		fmt.Println("Error writing tempFile - " + tempFileName + " | Error - " + err.Error())
		return nil, errors.New("error processing watermark")
	}
	wm, err := pdfcpu.ParseWatermarkDetails(waterMarkMessage, true)

	if err != nil {
		errMessage := "Error while creating wm - "
		fmt.Println(errMessage + err.Error())
		return nil, errors.New(errMessage)
	}

	if wm != nil {
		wm.Opacity = 0.40
		wm.Scale = 0.8
	}
	outputFile := randomStringForTempFiles + "_watermarked.pdf"

	pdfConf := pdfcpu.NewDefaultConfiguration()
	//TODO: We should get away from ValidationNone (currently, putting because we have seen some pdfs don't work fine without ValidationNone)
	pdfConf.ValidationMode = pdfcpu.ValidationNone
	wmError := pdfApi.AddWatermarksFile(tempFileName, outputFile, []string{}, wm, pdfConf)
	if wmError != nil {
		errMessage := "error while watermarking file"
		fmt.Println("error while watermarking file - " + tempFileName + " | Error - " + wmError.Error())
		return nil, errors.New(errMessage)
	}
	outputBytes, err := ioutil.ReadFile(outputFile)
	if err := os.Remove(tempFileName); err != nil {
		fmt.Println("Error while removing tempFile - " + tempFileName + " | Error - " + err.Error())
	}
	if err := os.Remove(outputFile); err != nil {
		fmt.Println("Error while removing tempFile - " + outputFile + " | Error - " + err.Error())
	}

	if err != nil {
		errMessage := "error reading watermarked file"
		fmt.Println(errMessage + " - " + err.Error())
		return nil, errors.New(errMessage)
	}
	return outputBytes, nil
}

func DefaultWaterMarkMessage(userIdentifierText string) string {
	return "Private & Confidential - " + userIdentifierText
}

func IsPdf(fileBytes []byte) bool {
	return http.DetectContentType(fileBytes) == ContentTypePdf
}
