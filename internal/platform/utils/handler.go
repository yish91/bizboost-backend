package utils

import (
	"bizboost/internal/platform/authentication"
	"encoding/json"
	"fmt"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
)

//validates the incoming http.Request, populates requestModel and runs the underlying serviceFunction
func RunWithValidationWithoutUser(w http.ResponseWriter, req *http.Request,
	requestModel interface{}, serviceFunction func() (response interface{}, err error)) {
	if err := validate(req, requestModel); err != nil {
		LogError(err)
		msg := err.Error()
		handleError(w, msg, http.StatusBadRequest)
		return
	}

	if response, err := serviceFunction(); err != nil {
		var statusCode int
		if err, ok := err.(*ServiceError); ok {
			statusCode = err.HttpStatusCode
		} else {
			statusCode = http.StatusInternalServerError
		}
		LogError(err)
		msg := err.Error()
		handleError(w, msg, statusCode)
		return
	} else {
		_ = json.NewEncoder(w).Encode(response)
		return
	}
}

//validates the incoming http.Request, populates requestModel and runs the underlying serviceFunction with authenticated userId
func RunWithValidationAndWithUser(w http.ResponseWriter, req *http.Request,
	requestModel interface{}, serviceFunction func(userId string) (response interface{}, err error)) {
	if err := validate(req, requestModel); err != nil {
		LogError(err)
		msg := err.Error()
		handleError(w, msg, http.StatusBadRequest)
		return
	}

	userId, err := authentication.GetUserId(req)
	if err != nil {
		LogError(err)
		msg := "Error obtaining userId!"
		handleError(w, msg, http.StatusUnauthorized)
		return
	}

	if response, err := serviceFunction(userId); err != nil {
		var statusCode int
		if err, ok := err.(*ServiceError); ok {
			statusCode = err.HttpStatusCode
		} else {
			statusCode = http.StatusInternalServerError
		}
		//LogError(err)
		msg := err.Error()
		handleError(w, msg, statusCode)
	} else {
		_ = json.NewEncoder(w).Encode(response)
	}
}

func RunWithoutValidationWithoutUser(w http.ResponseWriter, req *http.Request, serviceFunction func() (response interface{}, err error)) {
	if response, err := serviceFunction(); err != nil {
		var statusCode int
		if err, ok := err.(*ServiceError); ok {
			statusCode = err.HttpStatusCode
		} else {
			statusCode = http.StatusBadRequest
		}
		LogError(err)
		msg := err.Error()
		handleError(w, msg, statusCode)
	} else {
		_ = json.NewEncoder(w).Encode(response)
	}
}

func RunWithoutValidationWithUser(w http.ResponseWriter, req *http.Request, serviceFunction func(userId string) (response interface{}, err error)) {
	userId, err := authentication.GetUserId(req)
	if err != nil {
		LogError(err)
		msg := "Error obtaining userId!"
		handleError(w, msg, http.StatusUnauthorized)
		return
	}
	if response, err := serviceFunction(userId); err != nil {
		var statusCode int
		if err, ok := err.(*ServiceError); ok {
			statusCode = err.HttpStatusCode
		} else {
			statusCode = http.StatusInternalServerError
		}
		LogError(err)
		msg := err.Error()
		handleError(w, msg, statusCode)
	} else {
		_ = json.NewEncoder(w).Encode(response)
	}
}

func validate(req *http.Request, reqModel interface{}) error {
	err := json.NewDecoder(req.Body).Decode(&reqModel)
	if err != nil {
		LogError(err)
		return err
	}
	validate := validator.New()
	return validate.Struct(reqModel)
}

func handleError(w http.ResponseWriter, err string, code int) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	fmt.Fprintf(w, `{"error":%q}`, err)
}
