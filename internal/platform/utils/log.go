package utils

import (
	"bizboost/cmd/logger"
	"fmt"
	"github.com/pkg/errors"
	"runtime/debug"
	"strings"
)

type stackTracer interface {
	StackTrace() errors.StackTrace
}

func LogError(err error) error {

	log := logger.GetLogger()

	//stack := string(debug.Stack())
	//fmt.Println(strings.Replace(stack, "\n", "\r", -1))
	//logrus.WithFields(logrus.Fields{
	//	"stackTrace": stack,
	//}).Error(err)

	//wrappedError := errors.Wrap(err, "Logging")
	//
	//if stackErr, ok := wrappedError.(stackTracer); ok {
	//	log.WithField("stacktrace", stackErr.StackTrace()).Error(err)
	//}

	//stack := string(debug.Stack())
	//log.WithFields(logrus.Fields{
	//	"stackTrace": stack,
	//}).Error(err)

	stack := string(debug.Stack())
	fmt.Println(strings.Replace(stack, "\n", "\r", -1))
	log.WithField("timestamp", GetLocalisedDate()).Error(err)
	return err
}

func WrapError(err error, msg string) error {
	newError := errors.Wrap(err, msg)
	LogError(newError)
	return newError
}

func LogInfo(info string) {

	log := logger.GetLogger()

	//stack := string(debug.Stack())
	//fmt.Println(strings.Replace(stack, "\n", "\r", -1))
	//logrus.WithFields(logrus.Fields{
	//	"stackTrace": stack,
	//}).Error(err)

	//wrappedError := errors.Wrap(err, "Logging")
	//
	//if stackErr, ok := wrappedError.(stackTracer); ok {
	//	log.WithField("stacktrace", stackErr.StackTrace()).Error(err)
	//}

	//stack := string(debug.Stack())
	//log.WithFields(logrus.Fields{
	//	"stackTrace": stack,
	//}).Error(err)

	log.WithField("timestamp", GetLocalisedDate()).Info(info)
}
