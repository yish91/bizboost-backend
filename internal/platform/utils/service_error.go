package utils

type ServiceError struct {
	HttpStatusCode int
	ErrorMessage   string
}

func (error *ServiceError) Error() string {
	return error.ErrorMessage
}

func NewServiceError(statusCode int, errorMessage string) *ServiceError {
	return &ServiceError{statusCode, errorMessage}
}
