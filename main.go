package main

import (
	"bizboost/cmd"
	"bizboost/cmd/logger"
	"bizboost/cmd/settings"
	"net/http"
)

// our main function
func main() {
	settings.Init()
	logger.Init()
	cmd.Init()

	r := cmd.NewRouter()

	defer cmd.DisconnectDatabase()

	//if settings.GetEnvironment() == "local" {
	//	http.ListenAndServe(":8000", r)
	//} else {
	//	authentication.GenerateCert()
	//	server := &http.Server{
	//		Addr:    ":8000",
	//		Handler: r,
	//		TLSConfig: &tls.Config{
	//			MinVersion: tls.VersionTLS12,
	//		},
	//	}
	//	err := server.ListenAndServeTLS("cert.pem", "key.pem")
	//	if err != nil {
	//		log.Fatal(err)
	//	}
	//}
	http.ListenAndServe(":8000", r)
}
